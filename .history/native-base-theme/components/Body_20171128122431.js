import variable from './../variables/platform';

export default (variables = variable) => {
	const bodyTheme = {
		flex: 1,
		 justifyContent: "center", alignItems: "center",
		alignSelf: 'center',
	};

	return bodyTheme;
};
