import variable from './../variables/platform';

export default (variables = variable) => {
	const bodyTheme = {
		backgroundColor: 'yellow',
		flex: 1,
		alignItems: 'center',
		alignSelf: 'center',
		justifyContent: "center",
	};

	return bodyTheme;
};
