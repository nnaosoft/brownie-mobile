import { Platform, StatusBar, Text } from "react-native";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import LoginForm from "./Containers/LoginContainer";
import RegisterContainer from "./Containers/RegisterContainer";
import DrawerContainer from './Containers/DrawerContainer';
import styles from "./Components/styles";

// const DrawerStack = DrawerNavigator({
//   equipmentAdd: {
//     screen: EquipmentAddForm,
//     navigationOptions: {
//       headerTitle: "Equipment Add"
//     }
//   }
// }, {
//   gesturesEnabled: false,
//   contentComponent: (props) => <DrawerContainer {...props} />
// });

// const DrawerNavigation = StackNavigator(
//   {
//     DrawerStack: { screen: DrawerStack }
//   },
//   {
//     headerMode: "float",
//     navigationOptions: ({ navigation }) => ({
//       headerStyle: styles.navigationOptions.headerStyle,
//       headerTitleStyle: styles.navigationOptions.headerTitleStyle,
//       // title: "Logged In to your app!",
//       headerLeft: (
//         <Text onPress={() => navigation.navigate("DrawerToggle")}>Menu</Text>
//       )
//     })
//   }
// );

// login stack
const LoginStack = StackNavigator(
  {
    // login: {
    //   screen: LoginForm
    // },
    loginStack: { screen: RegisterContainer },
  },
  {
    headerMode: "float",
    navigationOptions: {
      header: null
    }
  }
);

const Router = StackNavigator(
  {
    loginStack: { screen: LoginStack },
    // drawerStack: { screen: DrawerNavigation }
  },
  {
    // Default config for all screens
    headerMode: "none",
    initialRouteName: "drawerStack",
    cardStyle: {
      paddingTop: Platform.OS === "ios" ? 0 : StatusBar.currentHeight
    }
  }
);

export default Router;
