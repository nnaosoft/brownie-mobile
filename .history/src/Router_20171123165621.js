import React, { Component } from "react";
import { Platform, StatusBar, Text } from "react-native";
import { Icon } from "native-base";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import LoginContainer from "./Containers/LoginContainer";
import RegisterContainer from "./Containers/RegisterContainer";
import DrawerContainer from './Containers/DrawerContainer';
import HomeContainer from "./Containers/HomeContainer";
import ProfileContainer from './Containers/ProfileContainer';
import FavoursListContainer from './Containers/FavoursListContainer';
import FovorDoneByMeContainer from "./Containers/FovorDoneByMeContainer";
import FovorDetailContainer from "./Containers/FovorDetailContainer";
import styles from "./styles";

const DrawerStack = DrawerNavigator(
  {
    home: {
      screen: HomeContainer,
      navigationOptions: {
        headerTitle: "Home"
      }
    },
    profile: {
      screen: ProfileContainer,
      navigationOptions: {
        headerTitle: "Profile"
      }
    },
    fovorAddedByMe: {
      screen: FavoursListContainer,
      navigationOptions: {
        headerTitle: "Favours List"
      }
    },
    fovorDoneByMe: {
      screen: FovorDoneByMeContainer,
      navigationOptions: {
        headerTitle: "To Do Favours"
      }
    },
    fovorDetail: {
      screen: FovorDetailContainer,
      navigationOptions: {
        headerTitle: "Favour Details"
      }
    }
  },
  {
    initialRouteName: "fovorDetail",
    gesturesEnabled: false,
    contentComponent: props => <DrawerContainer {...props} />
  }
);

const DrawerNavigation = StackNavigator(
  {
    DrawerStack: { screen: DrawerStack }
  },
  {
    headerMode: "float",
    navigationOptions: ({ navigation }) => ({
      // headerStyle: styles.navigationOptions.headerStyle,
      // headerTitleStyle: styles.navigationOptions.headerTitleStyle,
      // title: "Logged In to your app!",
      headerLeft: (
        <Text onPress={() => navigation.navigate("DrawerToggle")} style={{paddingLeft: 10}}>
          <Icon name="menu" />
        </Text>
      )
    })
  }
);

// login stack
const LoginStack = StackNavigator(
  {
    login: { screen: LoginContainer },
    register: { screen: RegisterContainer },
  },
  {
    headerMode: "float",
    navigationOptions: {
      header: null
    }
  }
);

const Router = StackNavigator(
  {
    loginStack: { screen: LoginStack },
    drawerStack: { screen: DrawerNavigation }
  },
  {
    // Default config for all screens
    headerMode: "none",
    initialRouteName: "drawerStack",
    cardStyle: {
      paddingTop: Platform.OS === "ios" ? 0 : StatusBar.currentHeight
    }
  }
);

export default Router;
