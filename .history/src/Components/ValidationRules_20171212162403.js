import { Toast } from "native-base";
import validate from "./validate";

export default function validateRules(
  fields,
  rules,
  // options = { format: "flat" }
) {
  const errors = validate(fields, rules, options);
  if (errors) {
    showErrors(errors);
  }
  return errors;
}

export function showErrors(errors) {
  const error = "";
  errors.forEach(function(value) {
    error += `\n${value}
                    `;
  });
  Toast.show({ text: error, type: "danger", duration: 3000 });
}

export const loginValidation = {
  email: {
    presence: true,
    // presence: {
    //   message: '^Please enter an email address'
    // },
    format: {
      pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      //   message: '^Please enter a valid email address'
    }
  },

  password: {
    presence: true,
    // presence: {
    //   message: '^Please enter a password'
    // },
    length: {
      minimum: 5,
      message: "must be at least 5 characters"
    }
  }
};

export const registerValidation = {
  firstName: {
    presence: {
      message: "^Please enter an first name"
    }
  },
  lastName: {
    presence: {
      message: "^Please enter an last name"
    }
  },
  phone: {
    presence: {
      message: "^Please enter an phone"
    }
    // format: {
    //   pattern: /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$,
    //   message: '^Please enter a valid phone'
    // }
  },

  email: {
    presence: {
      message: "^Please enter an email address"
    },
    format: {
      pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      message: "^Please enter a valid email address"
    }
  },

  password: {
    presence: {
      message: "^Please enter a password"
    },
    length: {
      minimum: 5,
      message: "^Your password must be at least 5 characters"
    }
  }
};
