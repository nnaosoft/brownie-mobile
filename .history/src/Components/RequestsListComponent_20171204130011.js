import React, { Component } from "react";
import { Image } from "react-native";
import { connect } from "react-redux";
import {
  Text,
  List,
  ListItem,
  Grid,
  Col,
  Row,
  Button,
} from "native-base";
import styles, { testImageURL } from "../styles";

class RequestsListComponent extends Component {
  onItemButtonPress() {
    this.props.navigation.navigate("fovorDetail");
  }

  renderListItem() {
    return <ListItem style={{ marginLeft: 0, paddingTop: 0, paddingBottom: 0 }
          // paddingLeft: 0
        } onPress={this.onItemButtonPress.bind(this)}>
        <Grid>
          <Col style={{ flex: 2, backgroundColor: "#00CE9F" }}>
            <Image source={{ uri: testImageURL }} style={{ // height: 200,
                width: null, flex: 1 }} />
          </Col>
          <Col style={{ flex: 4, paddingTop: 5, paddingBottom: 5, paddingLeft: 10 }}>
            <Row>
              <Col>
                <Text note>Name</Text>
                <Text> ABC </Text>
                <Text note style={{ alignSelf: "flex-start" }}>
                  Brownie Points
                </Text>
                <Text style={{ alignSelf: "flex-start" }}>ABC</Text>
              </Col>
              <Col>
                <Text note>Locations</Text>
                <Text>123</Text>
                <Text note />
                <Text />
              </Col>
            </Row>
            <Row>
              <Button success small>
                <Text uppercase={false}>Accept</Text>
              </Button>
              <Button small danger style={{ marginLeft: 5 }}>
                <Text uppercase={false}>Reject</Text>
              </Button>
            </Row>
          </Col>
        </Grid>
      </ListItem>;
  }

  render() {
    return <List>
          {this.renderListItem()}
          {this.renderListItem()}
      </List>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(RequestsListComponent);
