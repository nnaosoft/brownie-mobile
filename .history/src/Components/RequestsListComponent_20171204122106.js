import React, { Component } from "react";
import { Image } from "react-native";
import { connect } from "react-redux";
import {
  Text,
  List,
  ListItem,
  Grid,
  Col,
  Row,
  Button,
} from "native-base";
import styles from "../styles";

class RequestsListComponent extends Component {
  onItemButtonPress() {
    this.props.navigation.navigate("fovorDetail");
  }

  renderListItem() {
    return <ListItem style={{ marginLeft: 0, paddingTop: 0, paddingBottom: 0 }
          // paddingLeft: 0
        } onPress={this.onItemButtonPress.bind(this)}>
        <Grid>
          <Col style={{ backgroundColor: "#00CE9F" }}>
            <Image source={{ uri: "https://scontent.fkhi10-1.fna.fbcdn.net/v/t1.0-9/18581627_10209170464286529_2111883646500482439_n.jpg?oh=030374ea4eb7c64bb594034021c1116e&oe=5AA007B9" }} style={{ // height: 200,
                width: null, flex: 1 }} />
          </Col>
          <Col style={styles.listRow}>
            <Text style={{ alignSelf: "flex-start" }} note>Name</Text>
            <Text style={}> ABC </Text>
            <Text note>Brownie Points</Text>
            <Text>ABC</Text>
            <Button small>
              <Text>Accept</Text>
            </Button>
          </Col>
          <Col style={{ paddingTop: 5, paddingBottom: 5 }}>
            <Text note>Locations</Text>
            <Text>123</Text>
            <Text note />
            <Text />
            <Button small danger>
              <Text>Reject</Text>
            </Button>
          </Col>
        </Grid>
      </ListItem>;
  }

  render() {
    return <List>
          {this.renderListItem()}
          {this.renderListItem()}
      </List>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(RequestsListComponent);
