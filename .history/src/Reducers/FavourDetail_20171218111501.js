
const INITIAL_STATE = {
  id: 123,
  status: 1,
  error: "",
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    default:
      return state;
  }
};