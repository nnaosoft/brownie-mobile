
const INITIAL_STATE = {
  id: 123,
  status: 1,
  user: null,
  profile: null,
  token: null,
  error: "",
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    default:
      return state;
  }
};