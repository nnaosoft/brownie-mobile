import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Feather } from "@expo/vector-icons";
import {
  Container,
  Content,
  Row,
  Col,
  Button,
  Grid,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  Badge,
  Icon,
  Header,
  Title,
  Body,
  Footer,
  FooterTab,
  StyleProvider
} from "native-base";
import { Ionicons, FontAwesome } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class ProfileUpdateContainer extends Component {
  renderUpdateButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button info full >
        <Text uppercase={false} style={styles.authFooterText}>
          Update Info
        </Text>
      </Button>
    );
  }

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.navigate("profile")}>
              <Icon name="ios-arrow-back" />
            </Button>
            <Body>
              <Title>Profile </Title>
            </Body>
            <Button primary transparent>
            <Text />
              {/* <FontAwesome name="edit" color={commonColor.brandPrimary} size={25} /> */}
            </Button>
          </Header>
          <Content>
            <Form style={{ marginTop: 20 }}>
              <Item>
                <Input placeholder="Name" />
              </Item>
              <Item>
                <Feather name="at-sign" size={25} />
                <Input placeholder="Email" keyboardType="email-address" />
              </Item>
              <Item>
                <FontAwesome name="phone" size={25} />
                <Input placeholder="Phone" disabled={this.props.loading} keyboardType="phone-pad" />
              </Item>
              <Item>
                <FontAwesome name="lock" size={25} />
                <Input placeholder="Password" />
              </Item>
              <Item>
                <FontAwesome name="lock" size={25} />
                <Input placeholder="Confirm Password" secureTextEntry disabled={this.props.loading} />
              </Item>
              <Text style={{}}>{this.props.error}</Text>
            </Form>
          </Content>
          <Footer>
            <FooterTab>{this.renderUpdateButton()}</FooterTab>
          </Footer>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(ProfileUpdateContainer);
