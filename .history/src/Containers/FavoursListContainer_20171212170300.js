import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SimpleLineIcons, FontAwesome } from "@expo/vector-icons";
import {
  Root,
  Header,
  Title,
  Body,
  Content,
  Segment,
  Button,
  Text,
  StyleProvider
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FavoursListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: "pending" };
  }

  segmentTabPressed(tab) {
    this.setState({ activeTab: tab });
  }

  renderHeader() {
    return <Header>
            <Button primary transparent onPress={() => this.props.navigation.navigate("DrawerToggle")}>
              <SimpleLineIcons name="list" size={25} color={commonColor.brandPrimary} />
            </Button>
            <Body>
              <Title>Favours List </Title>
            </Body>
            <Button primary transparent>
              <Text />
            </Button>
          </Header>;
  }

  renderSegment(){
    return <Segment>
            <Button first  onPress={() => this.segmentTabPressed.bind(this)("pending")} active={this.state.activeTab == "pending" ? true : false}>
              <Text>Pending</Text>
            </Button>
            <Button  onPress={() => this.segmentTabPressed.bind(this)("active")} active={this.state.activeTab == "active" ? true : false}>
              <Text>Active</Text>
            </Button>
            <Button last  onPress={() => this.segmentTabPressed.bind(this)("completed")} active={this.state.activeTab == "completed" ? true : false}>
              <Text>Completed</Text>
            </Button>
          </Segment>;
  }

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Root>
            {this.renderHeader()}
            {this.renderSegment()}

          <Content>
            <ListViewComponent navigation={this.props.navigation} />
          </Content>
        </Root>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FavoursListContainer);
