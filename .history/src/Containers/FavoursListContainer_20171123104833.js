import React, { Component } from 'react';
import {
  Image
} from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Header,
  TabHeading,
  Segment,
  Button,
  Icon,
  Text,
  List,
  ListItem,
  Thumbnail,
  Row,
  Grid,
  Col,
} from "native-base";
import { Ionicons } from "@expo/vector-icons";

class FavoursListContainer extends Component {

  render() {
    return (
      <Container>
        <Segment>
          <Button first>
            <Text>Pending</Text>
          </Button>
          <Button>
            <Text>Active</Text>
          </Button>
          <Button last active>
            <Text>Completed</Text>
          </Button>
        </Segment>
        <Content textAlign='start'>
          <List>
            <ListItem>
              <Grid>
                <Col style={{ backgroundColor: '#00CE9F'  }}>
                < Image source = {
                  {
                    uri: 'https://scontent.fkhi10-1.fna.fbcdn.net/v/t1.0-9/18581627_10209170464286529_2111883646500482439_n.jpg?oh=030374ea4eb7c64bb594034021c1116e&oe=5AA007B9'
                  }
                }
                style = {
                  {
                    // height: 200,
                    width: null,
                    flex: 1
                  }
                }
                />
                </Col>
                <Col>
                  <Text note>Favour Name</Text>
                  < Text textAlign = 'left' > ABC < /Text>
                  <Text note>Pickup Location</Text>
                  <Text>ABC</Text>
                  <Text note>Uploaded By</Text>
                  <Text>ABC</Text>
                </Col>
                <Col >
                  <Text note>Brownie Points</Text>
                  <Text>123</Text>
                  <Text note>Dropoff Location</Text>
                  <Text>ABC</Text>
                  <Text note>Type</Text>
                  <Text>Pickup</Text>
                </Col>
              </Grid>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FavoursListContainer);
