import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Title,
  Body,
  Content,
  Segment,
  Button,
  Text,
  StyleProvider,
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FavoursListContainer extends Component {

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Header>
          <Button primary transparent onPress={() => this.props.navigation.navigate("DrawerToggle")}>
            <SimpleLineIcons name="list" size={25} color={commonColor.brandPrimary} />
          </Button>
          <Body>
            <Title>Favours List </Title>
          </Body>
          <Button primary transparent>
            {/* <FontAwesome name="filter" color={commonColor.brandPrimary} size={25} /> */}
          </Button>
        </Header>
        <Container>
          <Segment>
            <Button first>
              <Text>Pending</Text>
            </Button>
            <Button>
              <Text>Active</Text>
            </Button>
            <Button last active>
              <Text>Completed</Text>
            </Button>
          </Segment>
          <Content>
            <ListViewComponent navigation={this.props.navigation} />
          </Content>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FavoursListContainer);
