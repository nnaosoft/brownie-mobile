import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  H1,
  CheckBox,
  Body,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
// import {
//   emailChanged,
//   passwordChanged,
//   RegisterUser
// } from '../Actions';
import styles from "../styles";
// import { style } from "./styles/RegisterFormSytle";
// import getTheme from './native-base-theme/components';

class RegisterContainer extends Component {
  onEmailChanged(text) {
    // this.props.dispatch(emailChanged(text));
  }
  onPasswordChanged(text) {
    // this.props.dispatch(passwordChanged(text));
  }
  onRegisterButtonPress() {
    // const { email, password } = this.props;
    this.props.navigation.navigate("drawerStack");
    // this.props.dispatch(RegisterUser({ email, password }));
  }

  onLoginButtonPress() {
    this.props.navigation.navigate("login");
  }

  renderRegisterButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button
        info
        style={{}}
        onPress={this.onRegisterButtonPress.bind(this)}
        block
      >
        <Text style={{}}>Register </Text>
      </Button>
    );
  }

  render() {
    return <Container>
        <Content padder
         contentContainerStyle={{ justifyContent: 'center', flex: 1 }}
         enableAutoAutomaticScroll={true}
         >
          <Form>
              <H1 style={{textAlign: 'center'}}>
                Register
              </H1>
            <Item>
              <Label>First Name</Label>
              <Input placeholder="First Name" disabled={this.props.loading} />
            
            </Item>
            <Item>
              <Label>Last Name</Label>
              <Input disabled={this.props.loading} />
            </Item>
            <Item>
              <Label>Email</Label>
              <Input onChangeText={this.onEmailChanged.bind(this)} value={this.props.email} disabled={this.props.loading} />
            </Item>
            <Item>
              <Label>Phone Number</Label>
              <Input disabled={this.props.loading} />
            </Item>
            <Item>
              <Label>Password</Label>
              <Input onChangeText={this.onPasswordChanged.bind(this)} value={this.props.password} secureTextEntry disabled={this.props.loading} />
            </Item>
            <Item>
              <Label>Confirm Password</Label>
              <Input secureTextEntry disabled={this.props.loading} />
            </Item>
            <Item>
              <Label>City</Label>
              <Input disabled={this.props.loading} />
            </Item>
            <Item>
              <CheckBox checked={false}/>
              <Body>
                <Text>Accept terms and Conditions</Text>
              </Body>
            </Item>
            <Text style={{}}>{this.props.error}</Text>
            {this.renderRegisterButton()}
            <Button
              style={{marginTop: 10}}
              onPress={this.onLoginButtonPress.bind(this)}
              transparent
            >
              <Text style={{}}>Do you have alreay Account? Login </Text>
            </Button>
          </Form>
        </Content>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(RegisterContainer);
