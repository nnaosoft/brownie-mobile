import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SimpleLineIcons, Entypo } from "@expo/vector-icons";
import {
  Root,
  Header,
  Body,
  Title,
  Content,
  Segment,
  Button,
  Text,
  StyleProvider,
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FovorDoneByMeContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: "pending" };
  }

  segmentTabPressed(tab) {
    this.setState({ activeTab: tab });
  }
  
  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Root>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.navigate("DrawerToggle")}>
              <SimpleLineIcons name="list" size={25} color={commonColor.brandPrimary} />
            </Button>
            <Body>
              <Title>To Do Favours </Title>
            </Body>
            <Button primary transparent onPress={() => this.props.navigation.navigate("fovorCompletedContainer")}>
              <Entypo name="back-in-time" color={commonColor.brandPrimary} size={30} />
            </Button>
          </Header>
          <Segment>
            <Button first onPress={() => this.segmentTabPressed.bind(this)("pending")} active={this.state.activeTab == "pending" ? true : false}>
              <Text>Pending</Text>
            </Button>
            <Button last onPress={() => this.segmentTabPressed.bind(this)("ongoing")} active={this.state.activeTab == "ongoing" ? true : false}>
              <Text>On Going</Text>
            </Button>
          </Segment>
          <Content>
            <ListViewComponent navigation={this.props.navigation} />
          </Content>
        </Root>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorDoneByMeContainer);
