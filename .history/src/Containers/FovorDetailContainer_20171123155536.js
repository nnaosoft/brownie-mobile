import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image } from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Grid,
  Col,
  Right
} from "native-base";
import { testImageURL } from "../styles";
class FovorDetailContainer extends Component {

  render() {
    return <Container>
        <Content>
          <Card>
            <CardItem cardBody>
              <Image source={{ uri: testImageURL }} style={{ height: 200, width: null, flex: 1 }} />
            </CardItem>
            <CardItem>
              <Grid>
                <Col>
                  <Text note>Uploaded By</Text>
                  <Text>Richmond Mayour</Text>
                  <Text note>Favour Name</Text>
                  <Text>Pickup Cat</Text>
                </Col>
                <Col>
                </Col>
              </Grid>
            </CardItem>
          </Card>
        </Content>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorDetailContainer);
