import React, { Component } from "react";
import { connect } from "react-redux";
import Toast from "react-native-root-toast";
import {
  Root,
  Container,
  Content,
  Header,
  Body,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  H1,
  Footer,
  FooterTab,
  View,
  Icon,
  Title,
  StyleProvider
} from "native-base";
import { emailChanged, passwordChanged, loginUser } from "../Actions";
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";
import validateRules, {
  sendCodeValidation
} from "../Components/ValidationRules";

class SendCodeContainer extends Component {
  onSubmitButtonPress() {
    const errors = validateRules(this.props, registerValidation);
    if (!errors) {
      this.props.navigation.navigate("drawerStack");
    } else {
      Toast.show("Reset Code on your phone.", {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        onShow: () => {
          // calls on toast\`s appear animation start
        },
        onShown: () => {
          // calls on toast\`s appear animation end.
        },
        onHide: () => {
          // calls on toast\`s hide animation start.
        },
        onHidden: () => {
          // calls on toast\`s hide animation end.
        }
      });
      this.props.navigation.navigate("resetPassword");
    }
  }

  renderSubmitButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button info full onPress={this.onSubmitButtonPress.bind(this)}>
        <Text uppercase={false} style={styles.authFooterText}>
          Send Code
        </Text>
      </Button>
    );
  }

  render() {
    return (
      <StyleProvider style={getTheme(commonColor)}>
        <Root>
          <Container>
            <Header>
              <Body style={styles.headerBody}>
                <Title>Reset Password</Title>
              </Body>
            </Header>
            <Content
              padder
              contentContainerStyle2={{ justifyContent: "center", flex: 1 }}
            >
              <Form style={{ marginTop: 20 }}>
                <View>
                  <Input
                    placeholder="Enter your phone number"
                    value={this.props.phone}
                    keyboardType="phone-pad"
                    disabled={this.props.loading}
                  />
                </View>
                <Text style={{}}>{this.props.error}</Text>
              </Form>
            </Content>
            <Footer>
              <FooterTab>{this.renderSubmitButton()}</FooterTab>
            </Footer>
          </Container>
        </Root>
      </StyleProvider>
    );
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(SendCodeContainer);
