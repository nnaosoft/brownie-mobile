import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Segment,
  Button,
  Text,
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
class FovorDoneByMeContainer extends Component {

  render() {
    return (
      <Container>
        <Segment>
          <Button first>
            <Text>Pending</Text>
          </Button>
          <Button>
            <Text>Active</Text>
          </Button>
          <Button last active>
            <Text>Completed</Text>
          </Button>
        </Segment>
        <Content>
          <ListViewComponent />
        </Content>
      </Container>
    );
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorDoneByMeContainer);
