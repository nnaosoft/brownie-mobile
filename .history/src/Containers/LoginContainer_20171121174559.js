import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  H1,
  H2,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "./styles";
// import { style } from "./styles/LoginFormSytle";
// import getTheme from './native-base-theme/components';

class LoginForm extends Component {
  onEmailChanged(text) {
    this.props.dispatch(emailChanged(text));
  }
  onPasswordChanged(text) {
    this.props.dispatch(passwordChanged(text));
  }
  onLoginButtonPress() {
    const { email, password } = this.props;
    this.props.navigation.navigate("drawerStack");
    // this.props.dispatch(loginUser({ email, password }));
  }

  renderLoginButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button
        style={styles.submitButton}
        onPress={this.onLoginButtonPress.bind(this)}
        light
        block
      >
        <Text style={styles.submitButtonText}>Log In </Text>
      </Button>
    );
  }

  render() {
    return <Container style={styles.background}>
        <Content padder style={{ alignContent: "center", alignSelf: "center", marginTop: 110 }}>
          <Card style={{ paddingBottom: 5, backgroundColor: "#16364f" }}>
            <Form style={{ margin: 20, paddingTop: 60 }}>
              <Item regular style={styles.item}>
                <Ionicons name="ios-mail-outline" size={28} color="white" />
                <Label style={styles.label}>Email</Label>
                <Input style={{ color: "white" }} onChangeText={this.onEmailChanged.bind(this)} value={this.props.email} disabled={this.props.loading} />
              </Item>
              <Item regular style={styles.item}>
                <Ionicons name="ios-lock-outline" size={28} color="white" />
                <Label style={styles.label}>Password</Label>
                <Input style={{ color: "white" }} onChangeText={this.onPasswordChanged.bind(this)} value={this.props.password} secureTextEntry disabled={this.props.loading} />
              </Item>
              <Text style={{}}>{this.props.error}</Text>
              {this.renderLoginButton()}
              <Text style={{ fontSize: 20, fontWeight: "bold", color: "lightgrey", textAlign: "center" }}>
                START YOUR FREE 30 DAY TRAIL
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  fontWeight: "bold",
                  color: "white",
                  textAlign: "center"
                }}
              >
                FULL USE WITH NO LIMITS AND SUPPORT
              </Text>
              <H1
                style={{
                  fontSize: 20,
                  color: "white",
                  textAlign: "center"
                }}
              >
                CLICK HERE
              </H1>
            </Form>
          </Card>
        </Content>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(LoginForm);
