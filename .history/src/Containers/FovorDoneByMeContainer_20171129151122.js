import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Segment,
  Button,
  Text,
  StyleProvider,
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FovorDoneByMeContainer extends Component {

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
        <Segment>
          <Button first active>
            <Text>Pending</Text>
          </Button>
          <Button last>
            <Text>On Going</Text>
          </Button>
        </Segment>
        <Content>
          <ListViewComponent navigation={this.props.navigation} />
        </Content>
      </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorDoneByMeContainer);
