import React, { Component } from 'react';
import { Text, View, Image } from "react-native";
import { Container, Content } from "native-base";
import { NavigationActions } from "react-navigation";
import styles form '../styles';

export default class DrawerContainer extends Component {
  render() {
    const { navigation } = this.props;
    return <Container style={{ backgroundColor: "#352929" }}>
        <Content>
          <Text onPress={() => navigation.navigate("home")}>Home</Text>
          <Text onPress={() => navigation.navigate("favorAdd")}>
            Add a favor
          </Text>
          <Text onPress={() => navigation.navigate("fovorAddedByMe")}>
            Favours Added by me
          </Text>
          <Text onPress={() => navigation.navigate("fovorDoneByMe")}>
            Favours done by me
          </Text>
          <Text onPress={() => navigation.navigate("profile")}>
            My Profile
          </Text>
          <Text onPress={() => navigation.navigate("notifications")}>
            Notifications
          </Text>
          <Text onPress={() => navigation.navigate("myFriends")}>
            My Friends
          </Text>
          <Text onPress={() => navigation.navigate("messages")}>
            Messages
          </Text>
          <Text onPress={() => navigation.navigate("logout")}>Logout</Text>
        </Content>
      </Container>;
  }
}

