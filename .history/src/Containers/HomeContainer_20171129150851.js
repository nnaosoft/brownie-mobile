import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SimpleLineIcons } from "@expo/vector-icons";
import {
  Container,
  Content,
  Footer,
  FooterTab,
  Button,
  Text,
  Icon,
  StyleProvider
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class HomeContainer extends Component {

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container >
        <Content>
          <ListViewComponent navigation={this.props.navigation} />
        </Content>
        <Footer>
          <FooterTab>
            <Button vertical active>
              <Icon name="person" />
              <Text>Friends</Text>
            </Button>
            <Button vertical>
              <SimpleLineIcons name="location-pin" size={25} color="white" />
              <Text>Locations</Text>
            </Button>
            <Button vertical>
              <SimpleLineIcons name="list" size={25} color="white" />
              <Text>All Favours</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(HomeContainer);
