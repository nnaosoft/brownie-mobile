import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImagePicker } from "expo";
import { Image } from "react-native";
import {
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  Button,
  Spinner,
  Input,
  Text,
  Textarea,
  Form,
  Label,
  H2,
  H1,
  View,
  Grid,
  Row,
  Icon,
  StyleProvider
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FavourAddContainer extends Component {
  state = {
    image: null
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      // allowsEditing: true,
      aspect: [4, 3]
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

  renderHeaderLeft() {
    // return <Button style={{ backgroundColor: "blue" }} info transparent onPress={() => this.props.navigation.goBack()}>
    //     <Text uppercase={false}> Submit </Text>
    //   </Button>;

    return <Button info transparent onPress={() => this.props.navigation.goBack()}>
        <Icon name="arrow-back" />
      </Button>;
  }

  render() {
    let { image } = this.state;
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            {this.renderHeaderLeft()}
            <Body>
              <Title>Request Favour</Title>
            </Body>
              <Button info transparent>
                <Text uppercase={false}> Submit </Text>
              </Button>
          </Header>
          <Content>
            {!image && <View style={{ width: 400, height: 300, justifyContent: "center", alignItems: "center", backgroundColor: "#E5E5E5" }}>
                <View>
                  <Button onPress={this._pickImage} transparent>
                    <Text uppercase={false} style={{ fontWeight: "bold", fontSize: 20 }}>
                      Add a Photo
                    </Text>
                  </Button>
                </View>
                <View>
                  <Text>Favours with photos get completed faster!</Text>
                </View>
              </View>}
            {image && <View>
                <Image source={{ uri: image }} style={{ width: 400, height: 300 }} />
              </View>}

            <View style={{ margin: 15 }}>
              <View>
                <H2>FAVOUR INFO</H2>
              </View>
              <View>
                <Input placeholder="Favour title" />
              </View>
              <View>
                <Textarea placeholder="Enter a description for your favour" style={{ paddingLeft: 5 }} />
              </View>
              <View>
                <Input placeholder="Points" keyboardType="numeric" />
              </View>
              <View>
                <Input placeholder="Favor Complete Time" />
              </View>
              <View>
                <Input placeholder="Choose a Friend" />
              </View>
              <View>
                <H2>CONTACT & LOCATION</H2>
              </View>
              <View>
                <Input placeholder="+19027900636" />
              </View>
              <View>
                <Input placeholder="City Name" />
              </View>
              <Text style={{}}>{this.props.error}</Text>
            </View>
          </Content>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FavourAddContainer);
