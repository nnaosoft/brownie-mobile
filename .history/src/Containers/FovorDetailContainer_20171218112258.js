import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import {
  Container,
  Header,
  Body,
  Title,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  H3,
  Button,
  Icon,
  Grid,
  Col,
  Row,
  Footer,
  FooterTab,
  StyleProvider
} from "native-base";
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FovorDetailContainer extends Component {
  // constructor(){

  //   this.super();
  // }
  componentDidMount() {
                        // const { status } = this.props;
                        // console.log(this.props);
                        // this.props.dispatch(getFavourDetail({ id }));
                      }

  renderTimerRow() {
    // return;
    return <Row style={styles.timerRow}>
        <MaterialIcons name="timer" size={30} color={styles.timerNormalText.color} />
        <H3 style={styles.timerNormalText}>12:12</H3>
      </Row>;
  }

  renderSubmitButton(){
    return <Button info full>
        <Text uppercase={false} style={styles.authFooterText}>
          Accept Favour
        </Text>
      </Button>;
  }

  render() {
    const { status } = this.props.navigation.state.params.state;
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
            <Body>
              <Title>Favours Detail </Title>
            </Body>
            <Button primary transparent>
              <Text />
            </Button>
          </Header>
          <Content>
            <Card style={{ padding: 10 }}>
              <CardItem cardBody>
                <Image source={{ uri: styles.testImageURL }} style={{ height: 200, width: null, flex: 1 }} />
              </CardItem>
              <CardItem style={styles.favorDetailCardItem}>
                <Grid>
                  {status == 2 && this.renderTimerRow()}
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Uploaded By</Text>
                      <Text>Richmond Mayour</Text>
                    </Col>
                    <Col>
                      <Text note>Expiry Time</Text>
                      <Text>12:55 AM</Text>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Favour Name</Text>
                      <Text>Pickup Cat</Text>
                    </Col>
                    <Col>
                      <Text note>Brownie Points</Text>
                      <Text>25</Text>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Pickup Location</Text>
                      <Text>22 Street East</Text>
                    </Col>
                    <Col>
                      <Text note>Dropoff Location</Text>
                      <Text>12B, West Half</Text>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Desciption</Text>
                      <Text>
                        Mauris Vitae consecteur mauris, in porttitor odio.
                        Nullam elementum faucibus tempor. Vivamus ac
                        vestibulum enim. Vivamus nec urna felis. Nam
                        elementum nulla ac porta congue. Maecenas vitae
                        imperdiet orci, quis mattis purus.
                      </Text>
                    </Col>
                  </Row>
                </Grid>
              </CardItem>
            </Card>
          </Content>
          <Footer>
            <FooterTab>{this.renderSubmitButton()}</FooterTab>
          </Footer>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.favourDetail;
// };
// OR better is below, equal to above
const mapStateToProps = ({ favourDetail }) => {
  return favourDetail;
};

export default connect(mapStateToProps)(FovorDetailContainer);
