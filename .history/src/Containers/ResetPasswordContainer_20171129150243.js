import React, { Component } from "react";
import { connect } from "react-redux";
import Toast from "react-native-root-toast";
import {
  Container,
  Content,
  Header,
  Body,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  H1,
  Footer,
  FooterTab,
  View,
  Icon,
  Title,
  StyleProvider
} from "native-base";
import { emailChanged, passwordChanged, loginUser } from "../Actions";
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import platform from "../../native-base-theme/variables/platform";

class ResetPasswordContainer extends Component {
  onSubmitButtonPress() {
    Toast.show("Password has been updated.", {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      onShow: () => {
        // calls on toast\`s appear animation start
      },
      onShown: () => {
        // calls on toast\`s appear animation end.
      },
      onHide: () => {
        // calls on toast\`s hide animation start.
      },
      onHidden: () => {
        // calls on toast\`s hide animation end.
      }
    });
    this.props.navigation.navigate("login");
  }

  renderSubmitButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button onPress={this.onSubmitButtonPress.bind(this)}>
        <Text uppercase={false} style={styles.authFooterText}>
          Submit
        </Text>
      </Button>
    );
  }

  render() {
    return <StyleProvider style={getTheme(platform)}>
        <Container>
          <Header>
            <Body style={styles.headerBody}>
              <Title>Reset Password</Title>
            </Body>
          </Header>
          <Content padder contentContainerStyle2={{ justifyContent: "center", flex: 1 }}>
            <Form style={{ marginTop: 20 }}>
              <View>
                <Input placeholder="Enter Code number" value={this.props.code} keyboardType="numeric" disabled={this.props.loading} />
              </View>
              <View>
                <Input placeholder="Password" value={this.props.password} />
              </View>
              <View>
                <Input placeholder="Confirm Password" value={this.props.confirmPassword} />
              </View>
              <Text style={{}}>{this.props.error}</Text>
            </Form>
          </Content>
          <Footer>
            <FooterTab>{this.renderSubmitButton()}</FooterTab>
          </Footer>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(ResetPasswordContainer);
