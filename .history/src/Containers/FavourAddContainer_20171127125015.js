import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImagePicker } from "expo";
import { Image } from "react-native";
import {
  Container,
  Content,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Textarea,
  Form,
  Label,
  H1,
  View,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
// import { style } from "./styles/LoginFormSytle";
// import getTheme from './native-base-theme/components';

class FavourAddContainer extends Component {
  state = {
    image: null
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      // allowsEditing: true,
      aspect: [4, 3]
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

  render() {
    let { image } = this.state;
    return <Container>
        <Content padder>
          <Form style={{}}>
            <Item>
              <Button onPress={this._pickImage}>
                <Text>Pick an image from camera roll</Text>
              </Button>
            </Item>
            {image && <Item>
                <Image source={{ uri: image }} style={{ width: 400, height: 300 }} />
              </Item>}
            <Item>
              <Input placeholder="Favour Title" />
            </Item>
            <Item>
              <Textarea placeholder="Enter a description for your favour" style={{ flex: 1 }} />
            </Item>
            <Item>
              <Input placeholder="Points" keyboardType="numeric" />
            </Item>
            <Item>
              <Input placeholder="Favor Complete Time" />
            </Item>
            <Text style={{}}>{this.props.error}</Text>
            <Button info style={{ marginTop: 10 }} transparent />
          </Form>
        </Content>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FavourAddContainer);
