import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Content,
  Header,
  Body,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  H1,
  Footer,
  FooterTab,
  View,
  Icon,
  Title,
  Left,
  Right,
  StyleProvider
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import { emailChanged, passwordChanged, loginUser } from "../Actions";
import styles from "../styles";
import { style } from "expo/src/Font";
// import { style } from "./styles/LoginFormSytle";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class LoginContainer extends Component {
  onEmailChanged(text) {
    this.props.dispatch(emailChanged(text));
  }
  onPasswordChanged(text) {
    this.props.dispatch(passwordChanged(text));
  }
  onLoginButtonPress() {
    const { email, password } = this.props;
    this.props.navigation.navigate("drawerStack");
    // this.props.dispatch(loginUser({ email, password }));
  }
  onRegisterButtonPress() {
    this.props.navigation.navigate("register");
  }

  renderLoginButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button info onPress={this.onLoginButtonPress.bind(this)} full>
        <Text uppercase={false} style={styles.authFooterText}>
          Log In{" "}
        </Text>
      </Button>
    );
  }

  render() {
    return (
      <StyleProvider style={getTheme(commonColor)}>
        <Container style={{ backgroundColor: "white" }}>
          <Header>
            <Left></Left>
            <Body>
              <Title>Login</Title>
            </Body>
            <Right></Right>
          </Header>
          <Content
            padder
            contentContainerStyle2={{ justifyContent: "center", flex: 1 }}
          >
            <Form style={{ marginTop: 20 }}>
              <View>
                <Input
                  placeholder="Email"
                  onChangeText={this.onEmailChanged.bind(this)}
                  value={this.props.email}
                  keyboardType="email-address"
                  disabled={this.props.loading}
                />
              </View>
              <View>
                <Input
                  placeholder="Password"
                  onChangeText={this.onPasswordChanged.bind(this)}
                  value={this.props.password}
                  secureTextEntry
                  disabled={this.props.loading}
                />
              </View>
              <Text style={{}}>{this.props.error}</Text>
              <Button dark style={{ marginLeft: -10 }} transparent>
                <Text uppercase={false} style={{}}>
                  Forget Password?
                </Text>
              </Button>
            </Form>
          </Content>
          <View>
            <Button
              info
              style={{ alignSelf: "center", marginBottom: 40 }}
              onPress={this.onRegisterButtonPress.bind(this)}
              transparent
            >
              <Text uppercase={false} style={{ fontSize: 20 }}>
                Don't have an Account? Register Now
              </Text>
            </Button>
          </View>
          <Footer>
            <FooterTab>{this.renderLoginButton()}</FooterTab>
          </Footer>
        </Container>
      </StyleProvider>
    );
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(LoginContainer);
