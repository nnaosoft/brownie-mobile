import React, { Component } from 'react';
import { Text, View, Image } from "react-native";
import { Container, Content } from "native-base";
import { NavigationActions } from "react-navigation";
import styles from '../styles';

export default class DrawerContainer extends Component {
  render() {
    const { navigation } = this.props;
    return <Container style={{ backgroundColor: "#352929" }}>
        <Content style={{ color: "white" }}>
          <Text onPress={() => navigation.navigate("home")} style={styles.drawerItem}>
            Home
          </Text>
          <Text onPress={() => navigation.navigate("favorAdd")} style={styles.drawerItem}>
            > Add a favor
          </Text>
          <Text onPress={() => navigation.navigate("fovorAddedByMe")} style={styles.drawerItem}>
            > Favours Added by me
          </Text>
          <Text onPress={() => navigation.navigate("fovorDoneByMe")} style={styles.drawerItem}>
            > Favours done by me
          </Text>
          <Text onPress={() => navigation.navigate("profile")} style={styles.drawerItem}>
            > My Profile
          </Text>
          <Text onPress={() => navigation.navigate("notifications")} style={styles.drawerItem}>
            > Notifications
          </Text>
          <Text onPress={() => navigation.navigate("myFriends")} style={styles.drawerItem}>
            > My Friends
          </Text>
          <Text onPress={() => navigation.navigate("messages")} style={styles.drawerItem}>
            > Messages
          </Text>
          <Text onPress={() => navigation.navigate("loginStack")} style={styles.drawerItem}>
            > Logout
          </Text>
        </Content>
      </Container>;
  }
}

