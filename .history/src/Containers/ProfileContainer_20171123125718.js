import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image } from "react-native";
import StarRating from "react-native-star-rating";
import {
  Container,
  Content,
  Row,
  Button,
  Grid,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  H1,
  View,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
// import { style } from "./styles/LoginFormSytle";
// import getTheme from './native-base-theme/components';

class ProfileContainer extends Component {
  onStarRatingPress(rating) {
    
  }

  render() {
    return <Container>
        <Content>
          <Grid>
            <Row>
              <Image source={{ uri: "https://scontent.fkhi10-1.fna.fbcdn.net/v/t1.0-9/18581627_10209170464286529_2111883646500482439_n.jpg?oh=030374ea4eb7c64bb594034021c1116e&oe=5AA007B9" }} style={{ height: 400, width: null, flex: 1 }} />
            </Row>
            <Row>
              <Text style={{ fontSize: 40 }}>Adelle Leon</Text>
              <View>
                <StarRating emptyStar="star" emptyStarColor="lightblue" starColor="lightyellow" rating={3} selectedStar={rating => this.onStarRatingPress(rating)} />
              </View>
            </Row>
          </Grid>
        </Content>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(ProfileContainer);
