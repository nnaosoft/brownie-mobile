import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImageBackground } from "react-native";
import StarRating from "react-native-star-rating";
import { SimpleLineIcons, FontAwesome } from "@expo/vector-icons";
import {
  Container,
  Content,
  Row,
  Col,
  Button,
  Grid,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  Badge,
  View,
  Icon,
  Header,
  Title,
  Body,
  StyleProvider
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class ProfileUpdateContainer extends Component {
  onStarRatingPress(rating) {
    
  }

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.navigate("DrawerToggle")}>
              <SimpleLineIcons name="list" size={25} color={commonColor.brandPrimary} />
            </Button>
            <Body>
              <Title>Profile </Title>
            </Body>
            <Button primary transparent>
              <FontAwesome name="edit" color={commonColor.brandPrimary} size={25} />
            </Button>
          </Header>
          <Content>
            <Grid>
              <Row>
                <ImageBackground source={{ uri: "https://scontent.fkhi10-1.fna.fbcdn.net/v/t1.0-9/18581627_10209170464286529_2111883646500482439_n.jpg?oh=030374ea4eb7c64bb594034021c1116e&oe=5AA007B9" }} style={{ height: 400, width: null, flex: 1 }}>
                  <Icon name="camera" style={{ marginLeft: 10, marginTop: 10, fontSize: 40 }} />
                </ImageBackground>
              </Row>
              <Row style={{ margin: 20, paddingBottom: 20, borderBottomColor: "grey", borderBottomWidth: 1 }}>
                <Grid>
                  <Row>
                    <Text style={{ fontSize: 40 }}>Adelle Leon</Text>
                  </Row>
                  <Row>
                    <StarRating starSize={30} emptyStar="star" emptyStarColor="skyblue" rating={3} selectedStar={rating => this.onStarRatingPress(rating)} />
                    <Text style={{ fontSize: 20, marginLeft: 20 }}>
                      21 ratings
                    </Text>
                  </Row>
                </Grid>
              </Row>
              <Row style={{ marginRight: 50, marginLeft: 50, marginBottom: 20 }}>
                <Grid>
                  <Col>
                    <Button rounded info disabled>
                      <Text style={{ fontSize: 20 }}>2</Text>
                    </Button>
                    <Text style={{ fontSize: 20 }}>Current Points</Text>
                  </Col>
                  <Col>
                    <Button rounded info disabled>
                      <Text style={{ fontSize: 20 }}>2</Text>
                    </Button>
                    <Text style={{ fontSize: 20 }}>Total Points</Text>
                  </Col>
                  <Col>
                    <Button rounded info>
                      <Text style={{ fontSize: 20 }}>1234</Text>
                    </Button>
                    <Text style={{ fontSize: 20 }}>Brownie Friends</Text>
                  </Col>
                </Grid>
              </Row>
            </Grid>
          </Content>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(ProfileUpdateContainer);
