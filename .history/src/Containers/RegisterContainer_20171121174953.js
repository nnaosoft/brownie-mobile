import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  H1,
  H2,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
// import {
//   emailChanged,
//   passwordChanged,
//   RegisterUser
// } from '../Actions';
import styles from "./styles";
// import { style } from "./styles/RegisterFormSytle";
// import getTheme from './native-base-theme/components';

class RegisterContainer extends Component {
  onEmailChanged(text) {
    // this.props.dispatch(emailChanged(text));
  }
  onPasswordChanged(text) {
    // this.props.dispatch(passwordChanged(text));
  }
  onRegisterButtonPress() {
    // const { email, password } = this.props;
    // this.props.navigation.navigate("drawerStack");
    // this.props.dispatch(RegisterUser({ email, password }));
  }

  renderRegisterButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button
        style={styles.submitButton}
        onPress={this.onRegisterButtonPress.bind(this)}
        light
        block
      >
        <Text style={styles.submitButtonText}>Register </Text>
      </Button>
    );
  }

  render() {
    return (
      <Container >
        <Content
          padder
        >
            <Form style={{ margin: 20, paddingTop: 60 }}>
              <Item regular>
                <Label style={styles.label}>Email</Label>
                <Input
                  onChangeText={this.onEmailChanged.bind(this)}
                  value={this.props.email}
                  disabled={this.props.loading}
                />
              </Item>
              <Item regular>
                <Ionicons name="ios-lock-outline" size={28} color="white" />
                <Label style={styles.label}>Password</Label>
                <Input
                  style={{ color: "white" }}
                  onChangeText={this.onPasswordChanged.bind(this)}
                  value={this.props.password}
                  secureTextEntry
                  disabled={this.props.loading}
                />
              </Item>
              <Text style={{}}>{this.props.error}</Text>
              {this.renderRegisterButton()}
            </Form>
        </Content>
      </Container>
    );
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(RegisterForm);
