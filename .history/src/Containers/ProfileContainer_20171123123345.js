import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image } from "react-native";
import {
  Container,
  Content,
  Row,
  Button,
  Grid,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  H1,
  View,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
// import { style } from "./styles/LoginFormSytle";
// import getTheme from './native-base-theme/components';

class ProfileContainer extends Component {

  render() {
    return <Container>
        <Content>
          <Grid>
            <Row>
              <Image />
            </Row>
            <Row>
              <Image />
            </Row>
          </Grid>
        </Content>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(ProfileContainer);
