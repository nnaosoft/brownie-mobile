import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Image,
  CardItem,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  H1,
  View,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
// import { style } from "./styles/LoginFormSytle";
// import getTheme from './native-base-theme/components';

class ProfileContainer extends Component {
  onEmailChanged(text) {
    this.props.dispatch(emailChanged(text));
  }
  onPasswordChanged(text) {
    this.props.dispatch(passwordChanged(text));
  }
  onLoginButtonPress() {
    const { email, password } = this.props;
    // this.props.navigation.navigate("register");
    // this.props.dispatch(loginUser({ email, password }));
  }
  onRegisterButtonPress() {
    this.props.navigation.navigate("register");
  }

  renderLoginButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button
        onPress={this.onLoginButtonPress.bind(this)}
        block
      >
        <Text >Log In </Text>
      </Button>
    );
  }

  render() {
    return (
      <Container  >
        <Content >
            <Image />
        </Content>
      </Container>
    );
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(ProfileContainer);
