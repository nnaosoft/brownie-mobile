import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImageBackground } from "react-native";
import {
  Container,
  Content,
  Row,
  Col,
  Button,
  Grid,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  Badge,
  View,
  Icon,
  Header,
  Title,
  Body,
  StyleProvider
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class ProfileUpdateContainer extends Component {

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.navigate("DrawerToggle")}>
              <Icon name="ios-arrow-back" />
            </Button>
            <Body>
              <Title>Profile </Title>
            </Body>
            <Button primary transparent>
              {/* <FontAwesome name="edit" color={commonColor.brandPrimary} size={25} /> */}
            </Button>
          </Header>
          <Content>
            <Form style={{ marginTop: 20 }}>
              <View>
                <Input placeholder="Email" onChangeText={this.onEmailChanged.bind(this)} value={this.props.email} keyboardType="email-address" disabled={this.props.loading} />
              </View>
              <View>
                <Input placeholder="Password" onChangeText={this.onPasswordChanged.bind(this)} value={this.props.password} secureTextEntry disabled={this.props.loading} />
              </View>
              <Text style={{}}>{this.props.error}</Text>
              <Button dark style={{ marginLeft: -10 }} transparent onPress={this.onForgetPasswordButtonPress.bind(this)}>
                <Text uppercase={false} style={{}}>
                  Forget Password?
                </Text>
              </Button>
            </Form>
          </Content>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(ProfileUpdateContainer);
