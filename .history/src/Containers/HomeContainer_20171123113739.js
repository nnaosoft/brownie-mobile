import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Footer,
  FooterTab,
  Button,
  Text,
  Icon
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
class HomeContainer extends Component {

  render() {
    return <Container>
        <Content>
          <ListViewComponent />
        </Content>
        <Footer>
          <FooterTab>
            <Button vertical active>
              <Icon name="person" />
              <Text>Friends</Text>
            </Button>
            <Button vertical>
              <Icon name="apps" />
              <Text>Locations</Text>
            </Button>
            <Button vertical>
              <Icon name="list" />
              <Text>All Favours</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(HomeContainer);
