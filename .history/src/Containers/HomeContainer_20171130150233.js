import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SimpleLineIcons, FontAwesome } from "@expo/vector-icons";
import {
  Container,
  Content,
  Footer,
  FooterTab,
  Button,
  Text,
  Icon,
  Header,
  Body,
  Title,
  StyleProvider
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor, { brandPrimary } from "../../native-base-theme/variables/commonColor";

class HomeContainer extends Component {

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.navigate("DrawerToggle")}>
              <Icon name="menu" />
            </Button>
            <Body>
              <Title>Home {brandPrimary}</Title>
            </Body>
            <Button primary transparent>
              <FontAwesome name="filter" color={brandPrimary} size={25} />
            </Button>
          </Header>
          <Content>
            <ListViewComponent navigation={this.props.navigation} />
          </Content>
          <Footer>
            <FooterTab>
              <Button vertical active>
                <Icon name="person" />
                <Text>Friends</Text>
              </Button>
              <Button vertical>
                <SimpleLineIcons name="location-pin" size={25} color="white" />
                <Text>Locations</Text>
              </Button>
              <Button vertical>
                <SimpleLineIcons name="list" size={25} color="white" />
                <Text>All Favours</Text>
              </Button>
            </FooterTab>
          </Footer>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(HomeContainer);
