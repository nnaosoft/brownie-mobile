import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Header,
  Button,
  Spinner,
  View,
  Input,
  Text,
  Form,
  Label,
  H1,
  CheckBox,
  Body,
  Footer,
  FooterTab,
  Icon,
  Title,
  StyleProvider
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
// import {
//   emailChanged,
//   passwordChanged,
//   RegisterUser
// } from '../Actions';
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class RegisterContainer extends Component {
  onEmailChanged(text) {
    // this.props.dispatch(emailChanged(text));
  }
  onPasswordChanged(text) {
    // this.props.dispatch(passwordChanged(text));
  }
  onRegisterButtonPress() {
    // const { email, password } = this.props;
    this.props.navigation.navigate("drawerStack");
    // this.props.dispatch(RegisterUser({ email, password }));
  }

  onLoginButtonPress() {
    this.props.navigation.navigate("login");
  }

  renderRegisterButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button
        info
        style={{}}
        onPress={this.onRegisterButtonPress.bind(this)}
        block
      >
        <Text uppercase={false} style={styles.authFooterText}>Register </Text>
      </Button>
    );
  }
  renderAcceptCheckbox() {
    return <View>
        <CheckBox checked={false} />
        <Body>
          <Text>Accept terms and Conditions</Text>
        </Body>
      </View>;
  }

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container >
          <Header>
            <Body style={styles.headerBody}>
              <Title>Register</Title>
            </Body>
          </Header>
        <Content padder contentContainerStyle1={{ justifyContent: "center", flex: 1 }} enableAutoAutomaticScroll={true}>
          <Form>
            <View>
              <Input placeholder="First Name" disabled={this.props.loading} />
            </View>
            <View>
              <Input placeholder="Last Name" disabled={this.props.loading} />
            </View>
            <View>
              <Input placeholder="Email ID" onChangeText={this.onEmailChanged.bind(this)} value={this.props.email} keyboardType="email-address" disabled={this.props.loading} />
            </View>
            <View>
              <Input placeholder="Phone Number" disabled={this.props.loading} keyboardType="phone-pad" />
            </View>
            <View>
              <Input placeholder="Password" onChangeText={this.onPasswordChanged.bind(this)} value={this.props.password} secureTextEntry disabled={this.props.loading} />
            </View>
            <View>
              <Input placeholder="Confirm Password" secureTextEntry disabled={this.props.loading} />
            </View>
            <View>
              <Input placeholder="City" disabled={this.props.loading} />
            </View>
            <View>
              <Text style={{}}>{this.props.error}</Text>
            </View>
          </Form>
        </Content>
        <Footer>
          <FooterTab>{this.renderRegisterButton()}</FooterTab>
        </Footer>
      </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(RegisterContainer);
