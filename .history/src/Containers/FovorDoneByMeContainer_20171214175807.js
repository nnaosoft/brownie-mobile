import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SimpleLineIcons, Entypo } from "@expo/vector-icons";
import {
  Root,
  Header,
  Body,
  Title,
  Content,
  Segment,
  Button,
  Text,
  List,
  ListItem,
  StyleProvider,
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";
const tabValue = {
  PENDING: 1,
  ONGOING: 2,
}

class FovorDoneByMeContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: tabValue.PENDING};
  }

  segmentTabPressed(tab) {
    this.setState({ activeTab: tab });
  }

  renderSegment() {
    return <Segment>
            <Button first onPress={() => this.segmentTabPressed.bind(this)(tabValue.PENDING)} active={this.state.activeTab == tabValue.PENDING ? true : false}>
              <Text>Pending</Text>
            </Button>
            <Button last onPress={() => this.segmentTabPressed.bind(this)(tabValue.ONGOING)} active={this.state.activeTab == tabValue.ONGOING ? true : false}>
              <Text>On Going</Text>
            </Button>
          </Segment>;
  }
  renderHeader() {
    return <Header>
            <Button primary transparent onPress={() => this.props.navigation.navigate("DrawerToggle")}>
              <SimpleLineIcons name="list" size={25} color={commonColor.brandPrimary} />
            </Button>
            <Body>
              <Title>To Do Favours </Title>
            </Body>
            <Button primary transparent onPress={() => this.props.navigation.navigate("fovorCompletedContainer")}>
              <Entypo name="back-in-time" color={commonColor.brandPrimary} size={30} />
            </Button>
          </Header>;
  }
  
  renderPendingList() {
    if(this.state.activeTab != tabValue.PENDING) {
      return
    }
    return <ListViewComponent navigation={this.props.navigation} status={1} />;
  }

  renderOngoingList() {
    if(this.state.activeTab != tabValue.ONGOING) {
      return
    }
    return <List>
        <ListItem first>
          <Text>In Progress</Text>
        </ListItem>
        <ListViewComponent navigation={this.props.navigation} status={2} />
        <ListItem>
          <Text>Review Pending</Text>
        </ListItem>
        <ListViewComponent navigation={this.props.navigation} status={4} />
      </List>;
  }

  
  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Root>
            {this.renderHeader()}
            {this.renderSegment()}
          
          <Content>
            {this.renderPendingList()}
            {this.renderOngoingList()}
          </Content>
        </Root>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorDoneByMeContainer);
