import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SimpleLineIcons, FontAwesome } from "@expo/vector-icons";
import {
  Container,
  Content,
  Footer,
  FooterTab,
  Button,
  Text,
  Icon,
  Header,
  Body,
  Title,
  ActionSheet,
  Root,
  StyleProvider
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor, {
  brandPrimary,
  tabBarTextColor
} from "../../native-base-theme/variables/commonColor";
var BUTTONS = ["Pick Up/Drop Off", "Drive", "General", "Cancel"];
var CANCEL_INDEX = 3;

class HomeContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: 'friends',
    };
  }

  tabPressed(tab) {
    this.setState({ activeTab: tab });
  }

  render() {
    return (
      <StyleProvider style={getTheme(commonColor)}>
        <Root>
          <Container>
            <Header>
              <Button
                primary
                transparent
                onPress={() => this.props.navigation.navigate("DrawerToggle")}
              >
                <SimpleLineIcons
                  name="list"
                  size={25}
                  color={commonColor.brandPrimary}
                />
              </Button>
              <Body>
                <Title>Home </Title>
              </Body>
              <Button
                primary
                transparent
                onPress={() =>
                  ActionSheet.show(
                    {
                      options: BUTTONS,
                      cancelButtonIndex: CANCEL_INDEX,
                      title: "Search by Type"
                    },
                    buttonIndex => {
                      this.setState({ clicked: BUTTONS[buttonIndex] });
                    }
                  )
                }
              >
                <FontAwesome
                  name="filter"
                  color={commonColor.brandPrimary}
                  size={25}
                />
              </Button>
            </Header>
            <Content>
              <ListViewComponent navigation={this.props.navigation} />
            </Content>
            <Footer>
              <FooterTab>
                <Button
                  vertical
                  onPress={() => this.tabPressed.bind(this)("friends")}
                  active={this.state.activeTab == 'friends'? true : false}
                >
                  <Icon name="ios-person" />
                  <Text uppercase={false}>Friends</Text>
                </Button>
                <Button
                  vertical
                  onPress={() => this.tabPressed.bind(this)("locations")}
                  active={this.state.activeTab == 'locations'? true : false}
                >
                  <SimpleLineIcons
                    name="location-pin"
                    size={25}
                    color="#6b6b6b"
                  />
                  <Text uppercase={false}>Locations</Text>
                </Button>
                <Button
                  vertical
                  onPress={() => this.tabPressed.bind(this)("all")}
                  active={this.state.activeTab == 'all'? true : false}
                >
                  <SimpleLineIcons name="list" size={25} color="#6b6b6b" />
                  <Text uppercase={false}>All Favours</Text>
                </Button>
              </FooterTab>
            </Footer>
          </Container>
        </Root>
      </StyleProvider>
    );
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(HomeContainer);
