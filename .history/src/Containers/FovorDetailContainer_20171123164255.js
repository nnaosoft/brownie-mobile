import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image } from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Grid,
  Col,
  Row,
  Footer,
  FooterTab
} from "native-base";
import { testImageURL } from "../styles";
class FovorDetailContainer extends Component {

  render() {
    return <Container>
        <Content>
          <Card style={{ padding: 10 }}>
            <CardItem cardBody>
              <Image source={{ uri: testImageURL }} style={{ height: 200, width: null, flex: 1 }} />
            </CardItem>
            <CardItem style={{ borderWidth: 1, borderColor: "grey", marginTop: 10 }}>
              <Grid>
                <Row>
                  <Col>
                    <Text note>Uploaded By</Text>
                    <Text>Richmond Mayour</Text>
                    <Text note>Favour Name</Text>
                    <Text>Pickup Cat</Text>
                    <Text note>Pickup Location</Text>
                    <Text>No location</Text>
                  </Col>
                  <Col>
                    <Text note>Expiry Time</Text>
                    <Text>12:55</Text>
                    <Text note>Brownie Points</Text>
                    <Text>25</Text>
                    <Text note>Dropoff Location</Text>
                    <Text>No location</Text>
                  </Col>
                </Row>
                <Col>
                  <Row>
                    <Text note>Desciption</Text>
                  </Row>
                  <Row>
                    <Text>Test desciption for your a</Text>
                  </Row>
                </Col>
              </Grid>
            </CardItem>
          </Card>
        </Content>
        <Footer>
          <FooterTab>
            <Button info>
              <Text style={{ fontSize: 20 }}>Accept Favour</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorDetailContainer);
