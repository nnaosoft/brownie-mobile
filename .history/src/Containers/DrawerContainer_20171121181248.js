import React from "react";
import { Text, View, Image } from "react-native";
import { NavigationActions } from "react-navigation";
import styles form '../styles';
export default class DrawerContainer extends React.Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Text
          onPress={() => navigation.navigate("screen1")}
        >
          Dashboard
        </Text>
        <Text
          onPress={() => navigation.navigate("screen1")}
        >
          PM
        </Text>
        <Text
          onPress={() => navigation.navigate("screen1")}
        >
          Equipment
        </Text>
        <Text
          onPress={() => navigation.navigate("screen2")}
        >
          Location
        </Text>
        <Text
          onPress={() => navigation.navigate("screen3")}
        >
          Parts
        </Text>
        <Text
          onPress={() => navigation.navigate("screen3")}
        >
          Barcode Scanning
        </Text>
        <Text
          onPress={() => navigation.navigate("screen3")}
        >
          Logout
        </Text>
      </View>
    );
  }
}

