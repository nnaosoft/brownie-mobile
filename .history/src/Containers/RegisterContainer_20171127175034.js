import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Header,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  H1,
  CheckBox,
  Body,
  Footer,
  FooterTab,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
// import {
//   emailChanged,
//   passwordChanged,
//   RegisterUser
// } from '../Actions';
import styles from "../styles";
// import { style } from "./styles/RegisterFormSytle";
// import getTheme from './native-base-theme/components';

class RegisterContainer extends Component {
  onEmailChanged(text) {
    // this.props.dispatch(emailChanged(text));
  }
  onPasswordChanged(text) {
    // this.props.dispatch(passwordChanged(text));
  }
  onRegisterButtonPress() {
    // const { email, password } = this.props;
    this.props.navigation.navigate("drawerStack");
    // this.props.dispatch(RegisterUser({ email, password }));
  }

  onLoginButtonPress() {
    this.props.navigation.navigate("login");
  }

  renderRegisterButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button
        info
        style={{}}
        onPress={this.onRegisterButtonPress.bind(this)}
        block
      >
        <Text uppercase={false} style={styles.authFooterText}>Register </Text>
      </Button>
    );
  }

  render() {
    return <Container>
        <Header style={styles.authHeader}>
          <H1>Register</H1>
        </Header>
        <Content padder contentContainerStyle={{ justifyContent: "center", flex: 1 }} enableAutoAutomaticScroll={true}>
          <Form>
            <Item>
              <Input placeholder="First Name" disabled={this.props.loading} />
            </Item>
            <Item>
              <Input placeholder="Last Name" disabled={this.props.loading} />
            </Item>
            <Item>
              <Input placeholder="Email" onChangeText={this.onEmailChanged.bind(this)} value={this.props.email} keyboardType="email-address" disabled={this.props.loading} />
            </Item>
            <Item>
              <Input placeholder="Phone Number" disabled={this.props.loading} keyboardType="phone-pad" />
            </Item>
            <Item>
              <Input placeholder="Password" onChangeText={this.onPasswordChanged.bind(this)} value={this.props.password} secureTextEntry disabled={this.props.loading} />
            </Item>
            <Item>
              <Input placeholder="Confirm Password" secureTextEntry disabled={this.props.loading} />
            </Item>
            <Item>
              <Input placeholder="City" disabled={this.props.loading} />
            </Item>
            <Item>
              <CheckBox checked={false} />
              <Body>
                <Text>Accept terms and Conditions</Text>
              </Body>
            </Item>
            <Text style={{}}>{this.props.error}</Text>
          </Form>
        </Content>
        <Footer>
          <FooterTab>{this.renderRegisterButton()}</FooterTab>
        </Footer>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(RegisterContainer);
