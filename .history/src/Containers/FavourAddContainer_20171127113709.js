import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImagePicker } from "expo";
import {
  Container,
  Image,
  Content,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  H1,
  View,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
// import { style } from "./styles/LoginFormSytle";
// import getTheme from './native-base-theme/components';

class FavourAddContainer extends Component {
  state = {
    image: null
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3]
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

  onEmailChanged(text) {
    this.props.dispatch(emailChanged(text));
  }
  onPasswordChanged(text) {
    this.props.dispatch(passwordChanged(text));
  }
  onLoginButtonPress() {
    const { email, password } = this.props;
    this.props.navigation.navigate("drawerStack");
    // this.props.dispatch(loginUser({ email, password }));
  }
  onRegisterButtonPress() {
    this.props.navigation.navigate("register");
  }

  renderLoginButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button info onPress={this.onLoginButtonPress.bind(this)} block>
        <Text>Log In </Text>
      </Button>
    );
  }

  render() {
    let { image } = this.state;
    return (
      <Container>
        <Content padder>
          <Form style={{}}>
            <Item>
               <Button
          title="Pick an image from camera roll"
          onPress={this._pickImage}
        />
        {image &&
          <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
            <Item>
              <Input
                placeholder="Password"
                onChangeText={this.onPasswordChanged.bind(this)}
                value={this.props.password}
                secureTextEntry
                disabled={this.props.loading}
              />
            </Item>
            <Text style={{}}>{this.props.error}</Text>
            {this.renderLoginButton()}
            <Button
              info
              style={{ marginTop: 10 }}
              onPress={this.onRegisterButtonPress.bind(this)}
              transparent
            >
              <Text style={{}}>Don't have an Account? Register Now </Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FavourAddContainer);
