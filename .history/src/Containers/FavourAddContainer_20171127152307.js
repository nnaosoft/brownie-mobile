import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImagePicker } from "expo";
import { Image } from "react-native";
import {
  Container,
  Content,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Textarea,
  Form,
  Label,
  H1,
  View,
  Grid,
  Row,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
// import { style } from "./styles/LoginFormSytle";
// import getTheme from './native-base-theme/components';

class FavourAddContainer extends Component {
  state = {
    image: null
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      // allowsEditing: true,
      aspect: [4, 3]
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

  render() {
    let { image } = this.state;
    return <Container>
        <Content padder>
          <Form style={{}}>
            <Item style={{ width: 400, height: 300, justifyContent: "center", alignItems: "center", flex: 1 }}>
              <View style={{ flex: 2 }}>
                <Button onPress={this._pickImage} transparent>
                  <Text>Add a Photo</Text>
                </Button>
              </View>
              <View style={{ flex: 2 }}>
                <Text note>Favours with photos get completed faster!</Text>
              </View>
            </Item>
            {image && <Item>
                <Image source={{ uri: image }} style={{ width: 400, height: 300 }} />
              </Item>}
            <Item>
              <H1>FAVOUR INFO</H1>
            </Item>
            <Item>
              <Input placeholder="Favour Title" />
            </Item>
            <Item>
              <Textarea placeholder="Enter a description for your favour" style={{ flex: 1 }} />
            </Item>
            <Item>
              <Input placeholder="Points" keyboardType="numeric" />
            </Item>
            <Item>
              <Input placeholder="Favor Complete Time" />
            </Item>
            <Item>
              <Input placeholder="Choose a Friend" />
            </Item>
            <Item>
              <H1>CONTACT & LOCATION</H1>
            </Item>
            <Item>
              <Input placeholder="+19027900636" />
            </Item>
            <Item>
              <Input placeholder="City Name" />
            </Item>
            <Text style={{}}>{this.props.error}</Text>
          </Form>
        </Content>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FavourAddContainer);
