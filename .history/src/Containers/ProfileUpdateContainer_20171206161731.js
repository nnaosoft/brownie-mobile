import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImageBackground } from "react-native";
import {
  Container,
  Content,
  Row,
  Col,
  Button,
  Grid,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  Badge,
  View,
  Icon,
  Header,
  Title,
  Body,
  Footer,
  FooterTab,
  StyleProvider
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class ProfileUpdateContainer extends Component {
  renderUpdateButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button info full >
        <Text uppercase={false} style={styles.authFooterText}>
          Update Info
        </Text>
      </Button>
    );
  }

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.navigate("profile")}>
              <Icon name="ios-arrow-back" />
            </Button>
            <Body>
              <Title>Profile </Title>
            </Body>
            <Button primary transparent>
              {/* <FontAwesome name="edit" color={commonColor.brandPrimary} size={25} /> */}
            </Button>
          </Header>
          <Content>
            <Form style={{ marginTop: 20 }}>
              <View>
                <Input placeholder="Name" />
              </View>
              <View>
                <Input placeholder="Email" />
              </View>
              <View>
                <Input placeholder="Phone" disabled={this.props.loading} keyboardType="phone-pad" />
              </View>
              <View>
                <Input placeholder="Password" />
              </View>
              <View>
                <Input placeholder="Confirm Password" secureTextEntry disabled={this.props.loading} />
              </View>
              <Text style={{}}>{this.props.error}</Text>
            </Form>
          </Content>
          <Footer>
            <FooterTab>{this.renderUpdateButton()}</FooterTab>
          </Footer>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(ProfileUpdateContainer);
