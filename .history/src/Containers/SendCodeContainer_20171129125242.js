import React, { Component } from "react";
import { connect } from "react-redux";
import { Alert } from "react-native";
import {
  Container,
  Content,
  Header,
  Body,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  H1,
  Footer,
  FooterTab,
  View,
  Icon,
  Title,
  StyleProvider
} from "native-base";
import { emailChanged, passwordChanged, loginUser } from "../Actions";
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import platform from "../../native-base-theme/variables/platform";

class SendCodeContainer extends Component {

  onSubmitButtonPress() {
    Alert.prompt(
      'Update username',
      null,
      text => console.log("Your username is "+text),
      null,
      'default'
    );
    // this.props.navigation.navigate("resetPassword");
  }

  renderSubmitButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return <Button onPress={this.onSubmitButtonPress.bind(this)}>
        <Text uppercase={false} style={styles.authFooterText}>
          Send Code
        </Text>
      </Button>;
  }

  render() {
    return <StyleProvider style={getTheme(platform)}>
        <Container >
          <Header>
            <Body style={styles.headerBody}>
              <Title>Reset Password</Title>
            </Body>
          </Header>
          <Content padder contentContainerStyle2={{ justifyContent: "center", flex: 1 }}>
            <Form style={{ marginTop: 20 }}>
              <View>
                <Input placeholder="Enter your phone number" value={this.props.phone} keyboardType="phone-pad" disabled={this.props.loading} />
              </View>
              <Text style={{}}>{this.props.error}</Text>
            </Form>
          </Content>
          <Footer>
            <FooterTab>{this.renderSubmitButton()}</FooterTab>
          </Footer>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(SendCodeContainer);
