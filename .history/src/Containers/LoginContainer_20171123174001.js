import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Card,
  CardItem,
  Button,
  Spinner,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  H1,
  View,
  Icon
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
// import { style } from "./styles/LoginFormSytle";
// import getTheme from './native-base-theme/components';

class LoginContainer extends Component {
  onEmailChanged(text) {
    this.props.dispatch(emailChanged(text));
  }
  onPasswordChanged(text) {
    this.props.dispatch(passwordChanged(text));
  }
  onLoginButtonPress() {
    const { email, password } = this.props;
    this.props.navigation.navigate("drawerStack");
    // this.props.dispatch(loginUser({ email, password }));
  }
  onRegisterButtonPress() {
    this.props.navigation.navigate("register");
  }

  renderLoginButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button
        onPress={this.onLoginButtonPress.bind(this)}
        block
      >
        <Text >Log In </Text>
      </Button>
    );
  }

  render() {
    return <Container>
        <Content padder contentContainerStyle={{ justifyContent: "center", flex: 1 }}>
          <Form style={{}}>
            <H1 style={{ textAlign: "center" }}>Login</H1>
            <Item>
              <Input placeholder="Email" onChangeText={this.onEmailChanged.bind(this)} value={this.props.email} disabled={this.props.loading} />
            </Item>
            <Item>
              <Input placeholder="Password" onChangeText={this.onPasswordChanged.bind(this)} value={this.props.password} secureTextEntry disabled={this.props.loading} />
            </Item>
            <Text style={{}}>{this.props.error}</Text>
            {this.renderLoginButton()}
            <Button style={{ marginTop: 10, justifyContent: 'center' }} onPress={this.onRegisterButtonPress.bind(this)} transparent>
              <Text style={{}}>Don't have an Account? Register Now </Text>
            </Button>
          </Form>
        </Content>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(LoginContainer);
