import React, { Component } from 'react';
import { H1, View, Image } from "react-native";
import { Container, Content } from "native-base";
import { NavigationActions } from "react-navigation";
import styles from '../styles';

export default class DrawerContainer extends Component {
  render() {
    const { navigation } = this.props;
    return <Container style={{ backgroundColor: "#352929" }}>
        <Content style={{ color: "white" }}>
          <H1 onPress={() => navigation.navigate("home")} style={styles.drawerItem}>
            Home
          </H1>
          <H1 onPress={() => navigation.navigate("favorAdd")}>
            Add a favor
          </H1>
          <H1 onPress={() => navigation.navigate("fovorAddedByMe")}>
            Favours Added by me
          </H1>
          <H1 onPress={() => navigation.navigate("fovorDoneByMe")}>
            Favours done by me
          </H1>
          <H1 onPress={() => navigation.navigate("profile")}>
            My Profile
          </H1>
          <H1 onPress={() => navigation.navigate("notifications")}>
            Notifications
          </H1>
          <H1 onPress={() => navigation.navigate("myFriends")}>
            My Friends
          </H1>
          <H1 onPress={() => navigation.navigate("messages")}>
            Messages
          </H1>
          <H1 onPress={() => navigation.navigate("loginStack")}>
            Logout
          </H1>
        </Content>
      </Container>;
  }
}

