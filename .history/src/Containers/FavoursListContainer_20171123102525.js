import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Header,
  TabHeading,
  Segment,
  Button,
  Icon,
  Text,
  List,
  ListItem,
  Thumbnail,
  Row,
  Grid,
  Col,
} from "native-base";
import { Ionicons } from "@expo/vector-icons";

class FavoursListContainer extends Component {

  render() {
    return (
      <Container>
        <Segment>
          <Button first>
            <Text>Pending</Text>
          </Button>
          <Button>
            <Text>Active</Text>
          </Button>
          <Button last active>
            <Text>Completed</Text>
          </Button>
        </Segment>
        <List>
            <ListItem>
              <Grid>
                <Col style={{ backgroundColor: '#00CE9F'  }}>
                  <Thumbnail square size={80} source={{ uri: 'Image URL' }} />
                </Col>
                <Col>
                  <Text note>Favour Name</Text>
                  <Text>ABC</Text>
                  <Text note>Pickup Location</Text>
                  <Text>ABC</Text>
                  <Text note>Uploaded By</Text>
                  <Text>ABC</Text>
                </Col>
                <Col >
                  <Text note>Brownie Points</Text>
                  <Text>123</Text>
                  <Text note>Dropoff Location</Text>
                  <Text>ABC</Text>
                  <Text note>Type</Text>
                  <Text>Pickup</Text>
                </Col>
              </Grid>
            </ListItem>
          </List>
      </Container>
    );
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FavoursListContainer);
