import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImagePicker } from "expo";
import { Image } from "react-native";
import {
  Container,
  Content,
  Button,
  Spinner,
  Input,
  Text,
  Textarea,
  Form,
  Label,
  H2,
  H1,
  Header,
  View,
  Grid,
  Row,
  Icon,
  StyleProvider
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FavourAddContainer extends Component {
  state = {
    image: null
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      // allowsEditing: true,
      aspect: [4, 3]
    });

    console.log(result);

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

  render() {
    let { image } = this.state;
    return <StyleProvider style={getTheme(commonColor)}>
        <Container style={{ backgroundColor: "white" }}>
          <Header>
            <H1>Request Favour</H1>
          </Header>
          <Content padder>
            <Form style={{ marginBottom: 20 }}>
              {!image && <View style={{ width: 400, height: 300, justifyContent: "center", alignItems: "center", backgroundColor: "#E5E5E5" }}>
                  <View>
                    <Button info onPress={this._pickImage} transparent>
                      <Text uppercase={false} style={{ fontWeight: "bold", fontSize: 20 }}>
                        Add a Photo
                      </Text>
                    </Button>
                  </View>
                  <View>
                    <Text>Favours with photos get completed faster!</Text>
                  </View>
                </View>}
              {image && <View>
                  <Image source={{ uri: image }} style={{ width: 400, height: 300 }} />
                </View>}

              <View style={{ marginTop: 10 }}>
                <H2>FAVOUR INFO</H2>
              </View>
              <View>
                <Input placeholder="Favour title" />
              </View>
              <View>
                <Textarea placeholder="Enter a description for your favour" style={{ flex: 1 }} />
              </View>
              <View>
                <Input placeholder="Points" keyboardType="numeric" />
              </View>
              <View>
                <Input placeholder="Favor Complete Time" />
              </View>
              <View>
                <Input placeholder="Choose a Friend" />
              </View>
              <View>
                <H2>CONTACT & LOCATION</H2>
              </View>
              <View>
                <Input placeholder="+19027900636" />
              </View>
              <View>
                <Input placeholder="City Name" />
              </View>
              <Text style={{}}>{this.props.error}</Text>
            </Form>
          </Content>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FavourAddContainer);
