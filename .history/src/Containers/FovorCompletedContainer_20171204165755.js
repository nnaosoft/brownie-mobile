import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Body,
  Title,
  List,
  ListItem
  Content,
  Segment,
  Button,
  Text,
  Icon,
  StyleProvider,
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FovorCompletedContainer extends Component {

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.back()}>
              <Icon name="back" />
            </Button>
            <Body>
              <Title>Completed Favours </Title>
            </Body>
            <Button primary transparent>
              {/* <Entypo name="back-in-time" color={commonColor.brandPrimary} size={30} /> */}
            </Button>
          </Header>
          <Content>
            <List>
              <ListItem itemHeader first>
                <Text>Completed</Text>
              </ListItem>
              <ListViewComponent />
              <ListItem itemHeader>
                <Text>Disputed</Text>
              </ListItem>
              <ListViewComponent />
              <ListItem itemHeader>
                <Text>Cancelled</Text>
              </ListItem>
              <ListViewComponent />
            </List>
          </Content>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorCompletedContainer);
