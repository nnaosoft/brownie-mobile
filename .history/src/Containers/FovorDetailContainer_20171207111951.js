import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import {
  Container,
  Header,
  Body,
  Title,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  H3,
  Button,
  Icon,
  Grid,
  Col,
  Row,
  Footer,
  FooterTab,
  StyleProvider
} from "native-base";
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FovorDetailContainer extends Component {
  renderTimerRow() {
    // return;
    return <Row style={styles.timerRow}>
        <MaterialIcons name="timer" size={30} color="#2968B4" />
        <H3 style={{ color: "#2968B4" }}>12:12</H3>
      </Row>;
  }

  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
            <Body>
              <Title>Favours Detail </Title>
            </Body>
            <Button primary transparent>
              <Text />
            </Button>
          </Header>
          <Content>
            <Card style={{ padding: 10 }}>
              <CardItem cardBody>
                <Image source={{ uri: styles.testImageURL }} style={{ height: 200, width: null, flex: 1 }} />
              </CardItem>
              <CardItem style={styles.favorDetailCardItem}>
                <Grid>
                  {this.renderTimerRow()}
                  <Row style={{}}>
                    <Col>
                      <Text note>Uploaded By</Text>
                      <Text>Richmond Mayour</Text>
                      <Text note>Favour Name</Text>
                      <Text>Pickup Cat</Text>
                      <Text note>Pickup Location</Text>
                      <Text>No location</Text>
                    </Col>
                    <Col>
                      <Text note>Expiry Time</Text>
                      <Text>12:55</Text>
                      <Text note>Brownie Points</Text>
                      <Text>25</Text>
                      <Text note>Dropoff Location</Text>
                      <Text>No location</Text>
                    </Col>
                  </Row>
                  <Col>
                    <Row>
                      <Text note>Desciption</Text>
                    </Row>
                    <Row>
                      <Text>Test desciption for your a</Text>
                    </Row>
                  </Col>
                </Grid>
              </CardItem>
            </Card>
          </Content>
          <Footer>
            <FooterTab>
              <Button info>
                <Text uppercase={false} style={styles.authFooterText}>
                  Accept Favour
                </Text>
              </Button>
            </FooterTab>
          </Footer>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorDetailContainer);
