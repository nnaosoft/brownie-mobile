import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image } from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Grid,
  Col,
  Row,
  Footer,
  FooterTab
} from "native-base";
import { testImageURL } from "../styles";
class FovorDetailContainer extends Component {

  render() {
    return <Container>
        <Content>
          <Card>
            <CardItem cardBody>
              <Image source={{ uri: testImageURL }} style={{ height: 200, width: null, flex: 1 }} />
            </CardItem>
            <CardItem>
              <Grid>
                <Col>
                  <Text note>Uploaded By</Text>
                  <Text>Richmond Mayour</Text>
                </Col>
                <Col>
                  <Text note>Expiry Time</Text>
                  <Text>12:55</Text>
                </Col>
              </Grid>
            </CardItem>
            <CardItem>
              <Grid>
                <Col>
                  <Text note>Favour Name</Text>
                  <Text>Pickup Cat</Text>
                </Col>
                <Col>
                  <Text note>Brownie Points</Text>
                  <Text>25</Text>
                </Col>
              </Grid>
            </CardItem>
            <CardItem>
              <Grid>
                <Col>
                  <Text note>Pickup Location</Text>
                  <Text>No location</Text>
                </Col>
                <Col>
                  <Text note>Dropoff Location</Text>
                  <Text>No location</Text>
                </Col>
              </Grid>
            </CardItem>
            <CardItem>
              <Grid>
                <Row>
                  <Text note>Desciption</Text>
                </Row>
                <Row>
                  <Text>Test desciption for your a</Text>
                </Row>
              </Grid>
            </CardItem>
          </Card>
        </Content>
        <Footer>
          <FooterTab>
            <Button>
              <Text>Accept</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorDetailContainer);
