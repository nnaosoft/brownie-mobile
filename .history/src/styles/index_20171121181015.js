import { Platform, StatusBar, Text } from "react-native";

const textColor = '#FFFF';
const themeColor = "#16364f";
// const backGroundColor = "#16364f";

const styles = {
  background: {
    backgroundColor: 'black',
  },
  container: {
    flex: 1,
    backgroundColor: "#ffff",
    alignItems: "center",
    justifyContent: "center"
  },
  input: { color: textColor },
  item: { marginBottom: 5, padding: 10, height: 40, borderStyle: 'dotted', borderRadius: 0.5, borderWidth: 1 },
  twoItems: { marginBottom: 5, padding: 10, height: 40, flex:1, borderStyle: 'dotted', borderRadius: 0.5, borderWidth: 1  },
  label: { color: textColor, marginLeft: 5 },
  picker: {color: textColor, flex:1 },
  submitButton: { marginTop: 5, marginBottom: 10, height: 35 },
  submitButtonText: { fontSize: 15 },
  navigationOptions: {
    headerStyle: { backgroundColor: themeColor, color: textColor},
    headerTitleStyle: { color: textColor, justifyContent: 'center'}
  }
};

export default styles;
