import { Platform, StatusBar, Text } from "react-native";

const textColor = '#FFFF';
const themeColor = "#16364f";
// const backGroundColor = "#16364f";
export const testImageURL =
  "https://scontent.fkhi10-1.fna.fbcdn.net/v/t1.0-9/18581627_10209170464286529_2111883646500482439_n.jpg?oh=030374ea4eb7c64bb594034021c1116e&oe=5AA007B9"
const styles = {
  background: {
    backgroundColor: 'black',
  },
  container: {
    flex: 1,
    backgroundColor: "#ffff",
    alignItems: "center",
    justifyContent: "center"
  },
  input: { color: textColor },
  item: { marginBottom: 5, padding: 10, height: 40, borderStyle: 'dotted', borderRadius: 0.5, borderWidth: 1 },
  twoItems: { marginBottom: 5, padding: 10, height: 40, flex:1, borderStyle: 'dotted', borderRadius: 0.5, borderWidth: 1  },
  label: { color: textColor, marginLeft: 5 },
  picker: {color: textColor, flex:1 },
  submitButton: { marginTop: 5, marginBottom: 10, height: 35 },
  submitButtonText: { fontSize: 15 },
  navigationOptions: {
    headerStyle: { backgroundColor: themeColor, color: textColor},
    headerTitleStyle: { color: textColor, justifyContent: 'center'}
  }
};

export default styles;
