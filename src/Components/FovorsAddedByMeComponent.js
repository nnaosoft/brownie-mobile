import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  List,
  ListItem,
  Text,
  Icon,
} from "native-base";
import ListViewComponent from "./ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FovorsAddedByMeComponent extends Component {

  render() {
    return <List>
        <ListItem first>
          <Text>Completed</Text>
        </ListItem>
        <ListViewComponent navigation={this.props.navigation} status={6} />
        <ListItem>
          <Text>Disputed</Text>
        </ListItem>
        <ListViewComponent navigation={this.props.navigation} status={5} />
        <ListItem>
          <Text>Cancelled</Text>
        </ListItem>
        <ListViewComponent navigation={this.props.navigation} status={8} />
      </List>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorsAddedByMeComponent);
