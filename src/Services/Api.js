// a library to wrap and simplify api calls
import apisauce from 'apisauce';
import { store } from '../../App';

// our "constructor"
// const create = (baseURL = 'http://192.168.8.101/taskque_angular/api/') => {
// const create = (baseURL = 'http://192.168.0.216/taskque_angular/api/') => {
  const create = (baseURL = 'http://192.168.1.4/taskque_angular/api/') => {
  // ------
  // STEP 1
  // ------
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {},
    // 10 second timeout...
    timeout: 10000
  });

  // Wrap api's addMonitor to allow the calling code to attach
  // additional monitors in the future.  But only in __DEV__ and only
  // if we've attached Reactotron to console (it isn't during unit tests).
  // if (__DEV__ && console.tron) {
  //   api.addMonitor(console.tron.apisauce)
  // }

  // ------
  // STEP 2
  // ------
  //
  // Define some functions that call the api.  The goal is to provide
  // a thin wrapper of the api layer providing nicer feeling functions
  // rather than "get", "post" and friends.
  //
  // I generally don't like wrapping the output at this level because
  // sometimes specific actions need to be take on `403` or `401`, etc.
  //
  // Since we can't hide from that, we embrace it by getting out of the
  // way at this level.
  //
  const getHeaders = () => {
    const token = store.getState().auth.token;
    return (token) ? { Authorization: `Bearer ${token}` } : {};
  };
// api.defaults.headers.common['Authorization'] = 'AUTH_TOKEN' // need the token here

  // apisauce.defaults.headers = getHeaders();
  const getRoot = () => api.post('users/login', {}, { headers: { username: 'samar@mailinator.com', password: '123456' } });
//   const getRate = () => api.get('rate_limit')
//   const getUser = (username) => api.get('search/users', {q: username})
  const logintUser = ({ email, password }) => api.post('users/login', {}, { headers: { username: email, password } });
  const getWorkspacesList = () => api.get('workspaces', {}, { headers: getHeaders() });
  const getProjectList = (QueryString = {}) => api.get('projects', QueryString, { headers: getHeaders() });
  const getTaskList = (QueryString = {}) => api.get('tasks', QueryString, { headers: getHeaders() });
  const getTaskDetail = (QueryString = {}) => api.get(`tasks/${QueryString.id}`, QueryString, { headers: getHeaders() });
  const getTaskCommentList = (QueryString = {}) => api.get('taskcomments', QueryString, { headers: getHeaders() });
  
  // ------
  // STEP 3
  // ------
  //
  // Return back a collection of functions that we would consider our
  // interface.  Most of the time it'll be just the list of all the
  // methods in step 2.
  //
  // Notice we're not returning back the `api` created in step 1?  That's
  // because it is scoped privately.  This is one way to create truly
  // private scoped goodies in JavaScript.
  //
  return {
    // a list of the API functions from step 2
    getRoot,
    // getRate,
    // getUser,
    logintUser,
    getWorkspacesList,
    getProjectList,
    getTaskList,
    getTaskDetail,
    getTaskCommentList,
  };
};

// let's return back our create method as the default.
export default {
  create
};