import { createStore, applyMiddleware, compose } from 'redux';
// import createSagaMiddleware from 'redux-saga';
// import { offline } from 'redux-offline';
// import offlineConfig from 'redux-offline/lib/defaults';
import rootReducer from '../Reducers';
// import rootSaga from '../Sagas'; // TODO: Next step

//  Returns the store instance
// It can  also take initialState argument when provided
// const configureStore = () => {
//   const sagaMiddleware = createSagaMiddleware();
//   return {
//     ...createStore(rootReducer,
//     compose(
//       applyMiddleware(sagaMiddleware),
//       // offline(offlineConfig)
//     )),
//     runSaga: sagaMiddleware.run(rootSaga)
//   };
// };

const configureStore = () => {
  return {
    ...createStore(
      rootReducer
    )
  };
};
export default configureStore;
