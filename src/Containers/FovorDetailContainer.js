import React, { Component } from "react";
import { connect } from "react-redux";
import { Image } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import {
  Container,
  Header,
  Body,
  Title,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  H3,
  Button,
  Icon,
  Grid,
  Col,
  Row,
  Footer,
  FooterTab,
  StyleProvider
} from "native-base";
import styles from "../styles";
import * as CONSTANTS from "../Constants";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FovorDetailContainer extends Component {
  componentDidMount() {
    // const { status } = this.props;
    // console.log(this.props);
    // this.props.dispatch(getFavourDetail({ id }));
  }

  renderTimerRow(status) {
    if (status == 2) {
      return (
        <Row style={styles.timerRow}>
          <MaterialIcons
            name="timer"
            size={30}
            color={styles.timerNormalText.color}
          />
          <H3 style={styles.timerNormalText}>12:12</H3>
        </Row>
      );
    }
  }

  renderSubmitButton(status, user = 1) {
    if(!user){
      user = 1;
    }
    console.log(status, user);
    let buttonText = null;
      if(CONSTANTS.FAVOUR_STATUS_PENDING == status && user == 1) {
        buttonText = 'Cancel';
      }
      if (CONSTANTS.FAVOUR_STATUS_PENDING == status && user == 2) {
        buttonText = "Accept Favour";
      }
      if(CONSTANTS.FAVOUR_STATUS_INPROGRESS == status && user == 1) {
        buttonText = "Cancel"; // by Doer
      }
      if(CONSTANTS.FAVOUR_STATUS_INPROGRESS == status && user == 2) {
        buttonText = "Completed"; // by Doer
      }
      if(CONSTANTS.FAVOUR_STATUS_COMPLETED == status && user == 2) {
        buttonText = "Completed"; // by Creator
      }
      if(CONSTANTS.FAVOUR_STATUS_REVIEW_PENDING == status) {
        buttonText = "Review Pending";
      }
      if(CONSTANTS.FAVOUR_STATUS_DISPUTED == status) {
        buttonText = "Disputed";
      }
      if(CONSTANTS.FAVOUR_STATUS_EXPIRED== status) {
        buttonText = "Expired";
      }
      if(CONSTANTS.FAVOUR_STATUS_CANCELLED_BOFORE_START == status) {
        buttonText = "Cancelled";
      }
      if(CONSTANTS.FAVOUR_STATUS_CANCELLED_AFTER_START == status) {
        buttonText = "Cancelled";
      }

      if (buttonText) {
        return <Button info full>
            <Text uppercase={false} style={styles.authFooterText}>
              {buttonText}
            </Text>
          </Button>;
      }

  }

  render() {
    const { status, user } = this.props.navigation.state.params;
    return (
      <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Button
              primary
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon name="ios-arrow-back" />
            </Button>
            <Body>
              <Title>Favours Detail </Title>
            </Body>
            <Button primary transparent>
              <Text />
            </Button>
          </Header>
          <Content>
            <Card style={{ padding: 10 }}>
              <CardItem cardBody>
                <Image
                  source={{ uri: styles.testImageURL }}
                  style={{ height: 200, width: null, flex: 1 }}
                />
              </CardItem>
              <CardItem style={styles.favorDetailCardItem}>
                <Grid>
                  {status == 2 && this.renderTimerRow(status)}
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Uploaded By</Text>
                      <Text>Richmond Mayour</Text>
                    </Col>
                    <Col>
                      <Text note>Expiry Time</Text>
                      <Text>12:55 AM</Text>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Favour Name</Text>
                      <Text>Pickup Cat</Text>
                    </Col>
                    <Col>
                      <Text note>Brownie Points</Text>
                      <Text>25</Text>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Pickup Location</Text>
                      <Text>22 Street East</Text>
                    </Col>
                    <Col>
                      <Text note>Dropoff Location</Text>
                      <Text>12B, West Half</Text>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Desciption</Text>
                      <Text>
                        Mauris Vitae consecteur mauris, in porttitor odio.
                        Nullam elementum faucibus tempor. Vivamus ac vestibulum
                        enim. Vivamus nec urna felis. Nam elementum nulla ac
                        porta congue. Maecenas vitae imperdiet orci, quis mattis
                        purus.
                      </Text>
                    </Col>
                  </Row>
                </Grid>
              </CardItem>
            </Card>
          </Content>
          <Footer>
            <FooterTab>{this.renderSubmitButton(status, user)}</FooterTab>
          </Footer>
        </Container>
      </StyleProvider>
    );
  }
}

// const mapStateToProps = state => {
//   return state.favourDetail;
// };
// OR better is below, equal to above
const mapStateToProps = ({ favourDetail }) => {
  return favourDetail;
};

export default connect(mapStateToProps)(FovorDetailContainer);
