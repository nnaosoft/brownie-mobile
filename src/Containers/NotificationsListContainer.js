import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SimpleLineIcons, FontAwesome } from "@expo/vector-icons";
import {
  Container,
  Header,
  Title,
  Body,
  Thumbnail,
  Button,
  Text,
  List,
  ListItem,
  StyleProvider
} from "native-base";
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class NotificationsListContainer extends Component {

  renderHeader() {
    return <Header>
        <Button primary transparent>
          <Text />
        </Button>
        <Body>
          <Title>Notifications </Title>
        </Body>
        <Button primary transparent>
          <Text />
        </Button>
      </Header>;
  }

  renderItem(){
    return <ListItem>
        <Thumbnail size={80} source={{ uri: styles.testImageURL }} />
        <Body>
          <Text>Addele bel</Text>
          <Text note>Its time to build a difference . .</Text>
        </Body>
      </ListItem>;
  }
  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          {this.renderHeader()}
            <List>
              {this.renderItem()}
              {this.renderItem()}
              {this.renderItem()}
            </List>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(NotificationsListContainer);
