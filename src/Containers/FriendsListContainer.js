import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SimpleLineIcons, FontAwesome } from "@expo/vector-icons";
import {
  Container,
  Header,
  Title,
  Body,
  Thumbnail,
  Button,
  Text,
  List,
  ListItem,
  Icon,
  View,
  StyleProvider
} from "native-base";
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FriendsListContainer extends Component {
  
  onItemButtonPress() {
    this.props.navigation.navigate("userProfile");
  }

  renderHeader() {
    return (
      <Header>
        <Button primary transparent>
          <Text />
        </Button>
        <Body>
          <Title>Friends </Title>
        </Body>
        <Button primary transparent>
          <Text />
        </Button>
      </Header>
    );
  }

  renderItem() {
    return (
      <ListItem onPress={this.onItemButtonPress.bind(this)}>
        <Thumbnail size={80} source={{ uri: styles.testImageURL }} />
        <Body>
          <Text>Addele bel</Text>
        </Body>
        <Icon name="ios-remove-circle" style={{ color: "red" }} />
      </ListItem>
    );
  }
  render() {
    console.log(this.props.navigation);
    return (
      <StyleProvider style={getTheme(commonColor)}>
        <Container>
          {this.renderHeader()}
          <View padder>
            <Text>173 Friends</Text>
          </View>
          <List>
            {this.renderItem()}
            {this.renderItem()}
          </List>
        </Container>
      </StyleProvider>
    );
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FriendsListContainer);
