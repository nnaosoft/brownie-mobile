import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImagePicker } from "expo";
import { Image } from "react-native";
import {
  Root,
  Container,
  Content,
  Header,
  Left,
  Right,
  Body,
  Title,
  Button,
  ActionSheet,
  Spinner,
  Input,
  Text,
  Textarea,
  Form,
  Label,
  H2,
  H1,
  View,
  Grid,
  Row,
  Icon,
  Picker,
  Item,
  StyleProvider
} from "native-base";
import { Ionicons, SimpleLineIcons } from "@expo/vector-icons";
import {
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";
var BUTTONS = ["Take Photo", "Select Image", "Cancel"];
var CANCEL_INDEX = 2;

class FavourAddContainer extends Component {
  state = {
    image: null
  };
  
  _pickImage = async (buttonIndex) => {
    let result = {cancelled:true};
      if(buttonIndex == 0){
        result = await ImagePicker.launchCameraAsync({
          // allowsEditing: true,
          aspect: [4, 3]
        });
      } else
      if (buttonIndex == 1) {
       result = await ImagePicker.launchImageLibraryAsync({
          // allowsEditing: true,
          aspect: [4, 3]
        });
      }
      if (!result.cancelled) {
        this.setState({ image: result.uri });
      }
    };
  
  onValueChange(value: string) {
    this.setState({
      selectedFriend: value
    });
  }

  render() {
    let { image } = this.state;
    return <StyleProvider style={getTheme(commonColor)}>
    <Root>
        <Container>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.navigate("DrawerToggle")}>
              <SimpleLineIcons name="list" size={25} color={commonColor.brandPrimary} />
            </Button>
            <Body>
              <Title>Request Favour</Title>
            </Body>
            <Button primary transparent>
              <Text style={{paddingRight: 0, paddingLeft: 0}}  uppercase={false}>Submit</Text>
            </Button>
          </Header>
          <Content>
            {!image && <View style={{ width: 400, height: 300, justifyContent: "center", alignItems: "center", backgroundColor: "#E5E5E5" }}>
                <View>
                <Button primary transparent onPress={() => ActionSheet.show(
                {
                  options: BUTTONS,
                  cancelButtonIndex: CANCEL_INDEX,
                  title: "Select Photo"
                },
                buttonIndex => {
                  this._pickImage(buttonIndex);
                }
              )}>
                    <Text uppercase={false} style={{ fontWeight: "bold", fontSize: 20 }}>
                      Add a Photo
                    </Text>
                  </Button>
                </View>
                <View>
                  <Text>Favours with photos get completed faster!</Text>
                </View>
              </View>}
            {image && <View>
                <Image source={{ uri: image }} style={{ width: 400, height: 300 }} />
              </View>}

            <View style={{ margin: 15 }}>
              <View>
                <H2>FAVOUR INFO</H2>
              </View>
              <View>
                <Input placeholder="Favour title" />
              </View>
              <View>
                <Textarea placeholder="Enter a description for your favour" style={{ paddingLeft: 5 }} />
              </View>
              <View>
                <Input placeholder="Points" keyboardType="numeric" />
              </View>
              <View>
                <Input placeholder="Favor Complete Time" />
              </View>
              <View>
               <Picker
                  iosHeader="Choose a Friend"
                  mode="dropdown"
                  selectedValue={this.state.selectedFriend}
                  onValueChange={this.onValueChange.bind(this)}
                >
                  <Item label="Doland Trump" value="key0" />
                  <Item label="Bill Gates" value="key1" />
                  <Item label="Joe Roy" value="key2" />
                  <Item label="Mc Laurn" value="key3" />
                  <Item label="Steven Jobs" value="key4" />
                </Picker>
              </View>
              <View>
                <H2>CONTACT & LOCATION</H2>
              </View>
              <View>
                <Input placeholder="+19027900636" />
              </View>
              <View>
                <Input placeholder="City Name" />
              </View>
              <Text style={{}}>{this.props.error}</Text>
            </View>
          </Content>
        </Container>
        </Root>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FavourAddContainer);
