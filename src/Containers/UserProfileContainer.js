import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image } from "react-native";
import StarRating from "react-native-star-rating";
import { SimpleLineIcons, FontAwesome } from "@expo/vector-icons";
import {
  Container,
  Content,
  Row,
  Col,
  Button,
  Card,
  CardItem,
  Grid,
  Item,
  Input,
  Text,
  Form,
  Label,
  InputGroup,
  Badge,
  View,
  Icon,
  Header,
  Title,
  Body,
  StyleProvider
} from "native-base";
import {  
  emailChanged,
  passwordChanged,
  loginUser
} from '../Actions';
import styles from "../styles";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class UserProfileContainer extends Component {

  render() {
    console.log(this.props.navigation);
    return <StyleProvider style={getTheme(commonColor)}>
        <Container>
          <Header>
            <Button primary transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
            <Body>
              <Title>Ricky Jhon </Title>
            </Body>
            <Button primary transparent>
              <Image source={require("../../assets/icons/conversation-speech-bubbles.png")} style={{ height: 25, width: 30 }} />
            </Button>
          </Header>
          <Content>
            <Card style={{ padding: 10 }}>
              <CardItem cardBody>
                <Image source={{ uri: styles.testImageURL }} style={{ height: 200, width: null, flex: 1 }} />
              </CardItem>
              <CardItem style={styles.favorDetailCardItem}>
                <Grid>
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Phone</Text>
                      <Text>+19235478</Text>
                    </Col>
                    <Col>
                      <Text note>Rating</Text>
                      <View style={{ alignItems: "flex-start" }}>
                        <StarRating starSize={20} emptyStar="star" emptyStarColor={commonColor.brandPrimary} rating={3} />
                      </View>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Brownie Points Earned</Text>
                      <Text>120</Text>
                    </Col>
                    <Col>
                      <Text note>Brownie Points</Text>
                      <Text>25</Text>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: 10 }}>
                    <Col>
                      <Text note>Brownie Friends</Text>
                      <Text>45</Text>
                    </Col>
                    <Col>
                      <Text note />
                      <Text />
                    </Col>
                  </Row>
                </Grid>
              </CardItem>
            </Card>
          </Content>
        </Container>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(UserProfileContainer);
