import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Root,
  Header,
  Body,
  Title,
  List,
  ListItem,
  Content,
  Button,
  Text,
  Icon,
  StyleProvider,
} from "native-base";
import ListViewComponent from "../Components/ListViewComponent";
import getTheme from "../../native-base-theme/components";
import commonColor from "../../native-base-theme/variables/commonColor";

class FovorCompletedContainer extends Component {

  renderHeader() {
    return <Header>
            <Button primary transparent onPress={() => this.props.navigation.goBack("fovorCompletedContainer")}>
              <Icon name="ios-arrow-back" />
            </Button>
            <Body>
              <Title>Completed Favours </Title>
            </Body>
            <Button primary transparent>
              <Text />
            </Button>
          </Header>;
  }
  render() {
    return <StyleProvider style={getTheme(commonColor)}>
        <Root>
          {this.renderHeader()}
          <Content>
            <List>
              <ListItem first>
                <Text>Completed</Text>
              </ListItem>
              <ListViewComponent navigation={this.props.navigation} status={6} />
              <ListItem>
                <Text>Disputed</Text>
              </ListItem>
              <ListViewComponent navigation={this.props.navigation} status={5} />
              <ListItem>
                <Text>Cancelled</Text>
              </ListItem>
              <ListViewComponent navigation={this.props.navigation} status={8} />
            </List>
          </Content>
        </Root>
      </StyleProvider>;
  }
}

// const mapStateToProps = state => {
//   return state.auth;
// };
// OR better is below, equal to above
const mapStateToProps = ({ auth }) => {
  return auth;
};

export default connect(mapStateToProps)(FovorCompletedContainer);
