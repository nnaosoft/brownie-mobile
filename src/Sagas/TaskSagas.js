import { call, put } from 'redux-saga/effects';
// import LoginActions from '../Redux/LoginRedux'
import API from '../Services/Api';
import * as CONSTANTS from '../Constants';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create();

// attempts to login
export function* taskList(action) {
   const response = yield call(api.getTaskList, action.payload);

  if (response.ok && response.data.header.status === 200) {
    const payload = response.data.body;
    // do data conversion here if needed
    yield put({ type: CONSTANTS.TASK_LIST_FETCH_SUCCESS, payload });
  } else {
    yield put({ type: CONSTANTS.TASK_LIST_FETCH_FAIL });
  }
}

export function* taskDetail(action) {   
   const response = yield call(api.getTaskDetail, action.payload);

  if (response.ok && response.data.header.status === 200) {
    const payload = response.data.body;
    // do data conversion here if needed
    yield put({ type: CONSTANTS.TASK_DETAIL_FETCH_SUCCESS, payload });
  } else {
    yield put({ type: CONSTANTS.TASK_DETAIL_FETCH_FAIL });
  }
}
