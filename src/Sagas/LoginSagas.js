import { call, put, select } from 'redux-saga/effects';
// import LoginActions from '../Redux/LoginRedux'
import API from '../Services/Api';
import * as CONSTANTS from '../Constants';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create();

// attempts to login
export function* loginUser(action) {
  const { email, password } = action.payload;
  const response = yield call(api.logintUser, { email, password });
  if (response.ok && response.data.header.status == 200) {
    // const firstUser = path(['data', 'items'], response)[0]
    const payload = response.data.body;
    // do data conversion here if needed
    yield put({ type: CONSTANTS.LOGIN_USER_SUCCESS, payload });
    // const token = yield select(getHeader);
    // console.tron.log(token);
    // // const response2 = yield call(api.getWorkspacesList);
    // yield fork(workspacesList);
    // Actions.WorkspaceList();
  } else {
    const payload = response.data.header.message;
    yield put({ type: CONSTANTS.LOGIN_USER_FAIL, payload });
  }
}
const getAuth = state => state.auth;
export function* loginUserDetail(action) {
  const auth = yield select(getAuth);
  return auth;
}
