import { call, put } from 'redux-saga/effects';
// import LoginActions from '../Redux/LoginRedux'
import API from '../Services/Api';
import * as CONSTANTS from '../Constants';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create();

// attempts to login
export function* workspacesList(action) {
  console.tron.log(action);
   const response = yield call(api.getWorkspacesList);

  if (response.ok && response.data.header.status === 200) {
    const payload = response.data.body;
    // do data conversion here if needed
    yield put({ type: CONSTANTS.WORKSPACES_LIST_FETCH_SUCCESS, payload });
  } else {
    yield put({ type: CONSTANTS.WORKSPACES_LIST_FETCH_FAIL });
  }
}
