import { takeLatest, takeEvery } from 'redux-saga/effects';
import * as CONSTANTS from '../Constants';
import watcher from './watcher';

/* ------------- Sagas ------------- */

import { loginUser, loginUserDetail } from './LoginSagas';
import { workspacesList } from './WorkspaceSagas';
import { projectList } from './ProjectSagas';
import { taskList, taskDetail } from './TaskSagas';
import { taskCommentList } from './TaskCommentSagas';

/* ------------- Connect Types To Sagas ------------- */

export default function* rootSaga() {
// yield [
//   fork(watcher)
//  ];
  yield [
    // some sagas only receive an action
    takeLatest(CONSTANTS.LOGIN_USER, loginUser),
    takeLatest(CONSTANTS.LOGIN_USER_DETAIL, loginUserDetail),
    takeLatest(CONSTANTS.WORKSPACES_LIST_FETCH, workspacesList),
    takeLatest(CONSTANTS.PROJECT_LIST_FETCH, projectList),
    takeLatest(CONSTANTS.TASK_LIST_FETCH, taskList),
    takeLatest(CONSTANTS.TASK_DETAIL_FETCH, taskDetail),
    takeLatest(CONSTANTS.TASK_COMMENT_LIST_FETCH, taskCommentList),    
  ];
}
