import * as CONSTANTS from '../Constants';

const INITIAL_STATE = {
  tasks: {},
  error: '',
  loading: false,
  // page: 1,
  // project_id: null
};

export default (state = INITIAL_STATE, action) => {
  // console.log(state);
  const { Tasks, Project, Pagination } = state.tasks;
  switch (action.type) {
    case CONSTANTS.TASK_LIST_FETCH:
      return { ...state, loading: true, error: '' };
    case CONSTANTS.TASK_LIST_FETCH_SUCCESS:
      if (Project && Project.id === action.payload.Project.id && Pagination.page < action.payload.Pagination.page) {
        return {
        ...state,
        ...INITIAL_STATE,
        tasks: {
            Project: action.payload.Project,
            Pagination: action.payload.Pagination,
            Tasks: [...state.tasks.Tasks, ...action.payload.Tasks],
          }          
        };
      }
      return {
        ...state,
        ...INITIAL_STATE,
        tasks: action.payload
      };
    case CONSTANTS.TASK_LIST_FETCH_FAIL:
      return {
        ...state,
        error: 'List can not be fetched',
        loading: false
      };
    default:
      return state;
  }
};
