import * as CONSTANTS from '../Constants';

const INITIAL_STATE = {
  comments: { },
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CONSTANTS.TASK_COMMENT_LIST_FETCH:
      return { ...state, loading: true, error: '' };
    case CONSTANTS.TASK_COMMENT_LIST_FETCH_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        comments: action.payload
      };
    case CONSTANTS.TASK_COMMENT_LIST_FETCH_FAIL:
      return {
        ...state,
        error: 'List can not be fetched',
        loading: false
      };
    default:
      return state;
  }
};
