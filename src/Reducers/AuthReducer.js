import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL
} from '../Constants';

const INITIAL_STATE = {
  email: null,
  password: null,
  // user: null,
  profile: null,
  token: null,
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  // console.log(state);
  switch (action.type) {
    case EMAIL_CHANGED:
      return { ...state, email: action.payload };
    case PASSWORD_CHANGED:
      return { ...state, password: action.payload };
    case LOGIN_USER:
      return { ...state, loading: true, error: '' };
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        user: action.payload.User,
        profile: action.payload.Profile,
        token: action.payload.token
      };
    case LOGIN_USER_FAIL:
      return {
        ...state,
        error: action.payload,
        password: '',
        loading: false
      };
    default:
      return state;
  }
};
