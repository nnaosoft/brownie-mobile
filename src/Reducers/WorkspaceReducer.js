import * as CONSTANTS from '../Constants';

const INITIAL_STATE = {
  workspaces: [],
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CONSTANTS.WORKSPACES_LIST_FETCH:
      return { ...state, loading: true, error: '' };
    case CONSTANTS.WORKSPACES_LIST_FETCH_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        workspaces: action.payload
      };
    case CONSTANTS.WORKSPACES_LIST_FETCH_FAIL:
      return {
        ...state,
        error: 'List can not be fetched',
        loading: false
      };
    default:
      return state;
  }
};
