import * as CONSTANTS from '../Constants';

const INITIAL_STATE = {
  selectedTask: {},
  error: '',
  loading: false,
  // page: 1,
  // project_id: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CONSTANTS.TASK_DETAIL_FETCH:
      return { ...state, loading: true, error: '' };
    case CONSTANTS.TASK_DETAIL_FETCH_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        selectedTask: action.payload
      };
    case CONSTANTS.TASK_DETAIL_FETCH_FAIL:
      return {
        ...state,
        error: 'Task detail can not be fetched',
        loading: false
      };
    default:
      return state;
  }
};
