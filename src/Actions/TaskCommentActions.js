import * as CONSTANTS from '../Constants';

// export const workspaceNameChanged = text => {
//   return {
//     type: CONSTANTS.WORKSPACE_NAME_CHANGED,
//     payload: text
//   };
// };


export const getTaskComments = (payload) => {
  return {
    type: CONSTANTS.TASK_COMMENT_LIST_FETCH,
    payload
  };
};
