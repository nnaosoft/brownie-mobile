import * as CONSTANTS from '../Constants';

// export const workspaceNameChanged = text => {
//   return {
//     type: CONSTANTS.WORKSPACE_NAME_CHANGED,
//     payload: text
//   };
// };


export const getTaskList = (payload) => {
  return {
    type: CONSTANTS.TASK_LIST_FETCH,
    payload
  };
};

export const getTaskListDetail = (payload) => {
   return {
    type: CONSTANTS.TASK_DETAIL_FETCH,
    payload
  };
};
