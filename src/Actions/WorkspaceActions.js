import * as CONSTANTS from '../Constants';

// export const workspaceNameChanged = text => {
//   return {
//     type: CONSTANTS.WORKSPACE_NAME_CHANGED,
//     payload: text
//   };
// };


export const getWorkspaces = (payload) => {
  return {
    type: CONSTANTS.WORKSPACES_LIST_FETCH,
    payload
  };
};
