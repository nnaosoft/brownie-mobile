import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  FIRST_NAME_CHANGED,
  LAST_NAME_CHANGED,
  SKYPE_NAME_CHANGED,
  LOGIN_USER,
  LOGIN_USER_DETAIL,
  CONFIRM_PASSWORD_CHANGED,
} from '../Constants';

export const emailChanged = text => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const passwordChanged = text => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

export const confirmPasswordChanged = text => {
  return {
    type: CONFIRM_PASSWORD_CHANGED,
    payload: text
  };
};

export const firstNameChanged = text => {
  return {
    type: FIRST_NAME_CHANGED,
    payload: text
  };
};

export const lastChanged = text => {
  return {
    type: LAST_NAME_CHANGED,
    payload: text
  };
};

export const skypeNameChanged = text => {
  return {
    type: SKYPE_NAME_CHANGED,
    payload: text
  };
};

export const loginUser = (payload) => {
  return {
    type: LOGIN_USER,
    payload
  };
};

export const registerUser = payload => {
  return {
    type: LOGIN_USER,
    payload
  };
};

export const getloginUserDetail = payload => {
  return {
    type: LOGIN_USER_DETAIL,
    payload
  };
};

export const goRegisterScene = () => {
  // Actions.register();
};
export const goLoginScene = () => {
  // Actions.login();
};
export const goBackScene = () => {
  // Actions.pop();
};
