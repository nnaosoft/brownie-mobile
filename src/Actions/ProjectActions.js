import * as CONSTANTS from '../Constants';

export const getProjects = (payload) => {
  return {
    type: CONSTANTS.PROJECT_LIST_FETCH,
    payload
  };
};
